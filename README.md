# Search API Azure AI Search

This module provides a [Search API](https://www.drupal.org/project/search_api)
Backend for [Azure AI Search](https://azure.microsoft.com/en-us/products/ai-services/ai-search).

For a full description of the module, visit the [project page](https://www.drupal.org/project/search_api_aais).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/search_api_aais).

## Table of contents
 * 
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

## Requirements

This module requires the [Search API](https://www.drupal.org/project/search_api) module, as well as an admin key for the
configured search service you intend to use.

In addition, the submodule Search API Azure AI Search Autocomplete (search_api_aais_autocomplete) requires the [Search API Autocomplete](https://www.drupal.org/project/search_api_autocomplete) module.  
The submodule itself however, is optional.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

### Adding a new search server

- Navigate to Administration » Configuration » Search and metadata » Add server (/admin/config/search/search-api/add-server)
- Under "Backend", choose "Azure AI Search"
- Under "Azure AI Search Connector", choose "Standard (API key)"
- Configure the connector by filling in the "Url", "API-version" and "API key" settings

> At the moment you can only use an admin API key to connect to the server.

### Adding a new search index

- Navigate to Administration » Configuration » Search and metadata » Add index (/admin/config/search/search-api/add-index)
- Under "Server", choose the server created in the first step
- Configure the index by filling in the "Remote index name", and optionally check one of more of the additional options:
  - Check "Skip remote index creation", to not automatically create the remote index
  - Check "Delete pre-existing index", to delete an existing remote index with the same name
  - Check "Use a Synonym Map", to automatically create a synonym map based in the remote index's name
    - Depending on the chosen "API-version" of the server, you may need to enter a "Semantic configuration name"
  - Check "Search captions", if you wish to use Azure "Search captions" instead of the default Drupal excerpt

### Adding fields to the search index

- Navigate to Administration » Configuration » Search and metadata » Search API (/admin/config/search/search-api)
- Select the "Fields" operation for the desired index
- Add the desired fields, and configure the Azure-specific "Searchable", "Filterable", "Sortable", "Facetable", "Retrieveable" and "Language analyzer" settings for each field
  - Depending on the field "Type", certain settings might not be possible. This is an Azure limitation, as some field types cannot have certain configuration.
- Click on "Save changes".
  - After a field was added and saved, one or more of its Azure-specific settings might be locked. This is again an Azure limitation, as fields cannot have their settings changed after they have been added to an index.
  - If you do want to change a locked setting, simply remove the field and add it again.

### Adding a View

- Navigate to Administration » Structure » Views » Add view (/admin/structure/views/add)
- Configure the main View settings, but select "Index <index_name>" under the "Show" option of "View settings"
- Under Format » Format, choose either "Unformatted list", "HTML List" or "Grid"
- Under Format » Show, select "Rendered entity", as field-based rendering is currently unsupported
- Under Header or Footer, you can optionally add the "Semantic answer" field to show the semantic answer to a search query if available. Be sure to check the "Semantic scoring" option if you want users to be able to up - or downvote semantic answers

### Autocomplete

This feature requires the optional Search API Azure AI Search Autocomplete submodule.

- If the "Search API Azure AI Search Autocomplete" submodule was enabled, configure it by selecting the "Autocomplete" operation in Administration » Configuration » Search and metadata for the chosen index:
  - Check the View for which you want to configure the autocomplete functionality, and click "Edit".
  - Check the "Display Azure autosuggestions" under "Enabled suggesters".
  - Configure the suggester by (un)checking the "Use fuzzy suggestions" and "Use default index" options. If you uncheck the "Use default index" option, you can optionally provide an alternative remote index to query for the autocomplete suggestions.
  - Additionally, you may want to configure both the "Maximum number of suggestions" and "Minimum length of keywords for autocompletion" settings as well.

### Logging

This feature requires the optional Search API Azure AI Search Logging submodule.

- If the "Search API Azure AI Search Logging" submodule was enabled, configure it by editing the desired index:
  - Configure the logging and/or tracking by (un)checking the "Enable logging" and "Enable click tracking" options. Do note, that click tracking requires logging to be enabled.
  - If the "Enable click tracking" option is checked, configure the "Tracking selector" as needed. You can use this setting to target specific hyperlinks that needs to be tracked.

## Troubleshooting

Coming soon.

## FAQ

- Why do I not see a semantic answer, even though I added the "Semantic answer" field on my View?
  - For semantic answers to work properly, make sure that there is no HTML in the indexed value of the fields you wish to search in. If one of the indexed fields contain HTML by default ( like for example, a textarea with a WYSIWYG-editor enabled ) you will need to configure the "HTML filter" processor on the index.
- How can I see the results of the tracking and/or logging?
  - The database tables containing the tracking and logging information are exposed through Views. You can simply configure a new View and choose "Search API aais Logging" and/or "Search API aais Tracking" under View Settings » Show when creating the View.
- I enabled and configured the "Search API Azure AI Search Logging" submodule, but when I click a link in the search results the tracking information does not seem to be stored?
  - The "Search API Azure AI Search Logging" submodule adds specific data-attributes to the View rows of the "Unformatted list", "HTML List" and "Grid" View formats.<br />Make sure you have selected on of those formats, and that you have not stripped the View rows or their attributes in any custom templating you might have done.
- How can I use the Azure captions returned from a semantic search?
  - On the index configuration page, enable the "Search captions" option. If enabled, the "Search result excerpt" will be replaced by the Azure captions. So make sure the excerpt is configured on the appropriate View mode. Additionally, you can configure the HTML-tag used for the keyword highlighting within the captions.
- How can I see what query was sent to Azure?
  - Navigate to Administration » Configuration » Search and metadata » Search API » Azure AI Search configuration (/admin/config/search/search-api/azure-ai-search) and check the "Log queries" option

## Maintainers

This module was developed by [Anvil](https://www.anvil.be)
Please contact us for modifications. Patches always welcome.

- Justin Darge (vodde83) - https://www.drupal.org/u/vodde83
- Floris Walraet (interx) - https://www.drupal.org/u/interx
- Robin De Herdt (robindh) - https://www.drupal.org/u/robindh

This project has been sponsored by:
- [Stad Kortrijk](https://www.kortrijk.be)
- [Leiedal](https://www.leiedal.be)
