<?php

/**
 * @file
 * Hooks provided by the Search API Azure AI Search module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api_aais\BackendClient\ResponseDataInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the available analyzers.
 *
 * @param array $analyzers
 *   An associative array of analyzers, keyed by the type of analyzer.
 *
 * @see https://docs.microsoft.com/en-us/azure/search/index-add-custom-analyzers#built-in-analyzers
 * @see https://docs.microsoft.com/en-us/azure/search/index-add-language-analyzers#language-analyzer-list
 */
function hook_search_api_aais_analyzers_alter(array &$analyzers): void {
  // Add some additional uncommon language analyzers.
  $analyzers['language']['zh-Hans.microsoft'] = t('Chinese (Simplified)');
  $analyzers['language']['zh-Hant.microsoft'] = t('Chinese (Traditional)');

  // Unset the built-in analyzers.
  unset($analyzers['built-in']);
}

/**
 * Alter a parsed result form a search query.
 *
 * @param \Drupal\search_api\Query\QueryInterface $query
 *   The query.
 * @param \Drupal\search_api_aais\BackendClient\ResponseData $response
 *   Raw response from Azure.
 */
function hook_search_api_aais_query_results_alter(QueryInterface $query, ResponseDataInterface $response): void {
  $results = $query->getResults();
  $response_results = $response->getBody();
  $results->setExtraData('search_api_aais.my_data', $response_results['my_property'] ?? []);
}

/**
 * Alter the index settings form of an Azure AI Search index.
 *
 * @param array $form
 *   The third party settings form.
 * @param Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 * @param array $context
 *   An array that contains two properties:
 *     - configuration: the current index configuration
 *     - index: the Search API index we are altering of the type
 *       Drupal\search_api\IndexInterface.
 */
function hook_search_api_aais_index_settings_form_alter(array &$form, FormStateInterface $form_state, array $context): void {
  $configuration = $context['configuration'];
  $form['third_party_settings']['search_api_aais']['new_field'] = [
    '#type' => 'textfield',
    '#title' => t('New Field'),
    '#default_value' => $configuration['new_field'],
  ];
}

/**
 * Alter the query parameters of an Azure AI Search query.
 *
 * @param \Drupal\search_api\Query\QueryInterface $query
 *   The search query.
 * @param array $params
 *   The query parameters.
 */
function hook_search_api_aais_query_params_alter(QueryInterface $query, array &$params): void {
  if ($query->getIndex()->id() == 'my_index') {
    if (!empty($params['facets'])) {
      foreach ($params['facets'] as &$facet) {
        if ($facet == 'my_field') {
          // Limit the amount of items in the facet to a number of values.
          $facet .= ',count:10';
        }
      }
    }
  }

  if ($query->getIndex()->id() == 'my_other_index') {
    $params['queryType'] = 'full';
  }
}
