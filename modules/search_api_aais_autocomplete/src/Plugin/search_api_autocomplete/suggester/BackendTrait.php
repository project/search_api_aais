<?php

namespace Drupal\search_api_aais_autocomplete\Plugin\search_api_autocomplete\suggester;

use Drupal\Core\Utility\Error;
use Drupal\search_api\IndexInterface;
use Drupal\search_api_aais\AzureAiSearchBackendInterface;

/**
 * Trait that adds backend manipulation methods.
 */
trait BackendTrait {

  /**
   * Retrieves the backend for the given index, if it supports autocomplete.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search index.
   *
   * @return \Drupal\search_api_aais\AzureAiSearchBackendInterface|null
   *   The backend plugin of the index's server, if it exists and supports
   *   autocomplete; NULL otherwise.
   */
  protected static function getBackend(IndexInterface $index): ?AzureAiSearchBackendInterface {
    try {
      if (
        $index->hasValidServer() &&
        ($server = $index->getServerInstance()) &&
        ($backend = $server->getBackend()) &&
        $backend instanceof AzureAiSearchBackendInterface &&
        $server->supportsFeature('search_api_autocomplete')
      ) {
        return $backend;
      }
    }
    catch (\Exception $e) {
      Error::logException(\Drupal::logger('search_api_aais'), $e);
    }
    return NULL;
  }

}
