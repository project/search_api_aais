<?php

namespace Drupal\search_api_aais_autocomplete\Plugin\search_api_autocomplete\suggester;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;
use Drupal\search_api_aais\Azure\Query\QueryFilterBuilderInterface;
use Drupal\search_api_aais\AzureAiSearchBackendInterface;
use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\search_api_autocomplete\Suggester\SuggesterPluginBase;
use Drupal\search_api_autocomplete\Suggestion\SuggestionFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a suggester plugin that displays Azure autosuggestions.
 *
 * @SearchApiAutocompleteSuggester(
 *   id = "azure_ai_search",
 *   label = @Translation("Display Azure AI search autosuggestions"),
 *   description = @Translation("Display Azure AI search autosuggestion results to visitors as they type."),
 * )
 */
class AzureAiSearch extends SuggesterPluginBase implements PluginFormInterface {

  use BackendTrait;

  /**
   * The Search API Azure AI Search query filter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryFilterBuilderInterface
   *   The query filter builder.
   */
  protected QueryFilterBuilderInterface $queryFilterBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $plugin */
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $plugin->setQueryFilterBuilder($container->get('search_api_aais.query_filter_builder'));

    return $plugin;
  }

  /**
   * Retrieves query filter builder.
   *
   * @return \Drupal\search_api_aais\Azure\Query\QueryFilterBuilderInterface
   *   The query filter builder.
   */
  public function getQueryFilterBuilder(): QueryFilterBuilderInterface {
    return $this->queryFilterBuilder;
  }

  /**
   * Sets the query filter builder.
   *
   * @param \Drupal\search_api_aais\Azure\Query\QueryFilterBuilderInterface $query_filter_builder
   *   The query filter builder.
   *
   * @return $this
   */
  public function setQueryFilterBuilder(QueryFilterBuilderInterface $query_filter_builder): self {
    $this->queryFilterBuilder = $query_filter_builder;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'fuzzy' => FALSE,
      'index_as_default' => TRUE,
      'index_as_name' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['fuzzy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use fuzzy suggestions'),
      '#description' => $this->t('This provides a better experience in some scenarios but it comes at a performance cost as fuzzy suggestion searches are slower and consume more resources.'),
      '#default_value' => $this->configuration['fuzzy'],
    ];

    $form['index_as_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use default index'),
      '#description' => $this->t('Uncheck this to configure an alternative index for the autosuggestions.'),
      '#default_value' => $this->configuration['index_as_default'],
    ];

    $form['index_as_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autosuggestions index name'),
      '#description' => $this->t('The name of the remote Azure index to query for the autosuggestions.'),
      '#default_value' => $this->configuration['index_as_name'],
      '#states' => [
        'visible' => [
          ':input[name$="[index_as_default]"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name$="[index_as_default]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    if (!$values['index_as_default']) {
      // Make sure an alternative index name is given.
      if (empty($values['index_as_name'])) {
        $form_state->setError($form['index_as_name'], $this->t('@name field is required.', [
          '@name' => $form['index_as_name']['#title'],
        ]));
      }
      else {
        // If an alternative index is to be used for the autocomplete, make it
        // exists remotely.
        $index = $values['index_as_name'];

        /** @var \Drupal\Core\Entity\EntityFormInterface $form_object */
        $form_object = $form_state->getFormObject();
        /** @var \Drupal\search_api_autocomplete\Entity\Search $search */
        $search = $form_object->getEntity();

        /** @var \Drupal\search_api_aais\Plugin\search_api\backend\SearchApiAzureAiSearchBackend $backend */
        $backend = $search->getIndex()->getServerInstance()->getBackend();

        if (!$backend->getClient()->indexExists($index)) {
          $form_state->setError($form['index_as_name'], $this->t('The index %index does not exist on the remote server.', [
            '%index' => $index,
          ]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocompleteSuggestions(QueryInterface $query, $incomplete_key, $user_input): array {
    $suggestions = [];
    if ($backend = static::getBackend($query->getIndex())) {
      /** @var \Drupal\search_api_aais\AzureAiSearchBackendInterface $backend */
      // Set some default autocomplete parameters.
      $params = [
        'fuzzy' => (bool) $this->configuration['fuzzy'],
        'search' => $user_input,
        'suggesterName' => 'sg',
        'top' => $this->search->getOption('limit'),
      ];

      // Add a language filter to the conditions group if applicable.
      $languages = $query->getLanguages();
      if ($languages !== NULL) {
        $query->getConditionGroup()->addCondition('search_api_language', $languages, 'IN');
      }

      // Construct the filters.
      /** @var \Drupal\search_api_aais\Azure\Query\QueryFilterBuilder $filter_builder */
      $filter_builder = $this->getQueryFilterBuilder();
      $filters = $filter_builder->buildQueryFilters($query->getConditionGroup(), IndexBuilder::getIndexFields($query->getIndex()));

      if (!empty($filters)) {
        $params['filter'] = implode(' and ', $filters);
      }

      // Get the name of the index to be queried, as this can be overridden
      // through configuration.
      $index_name = !$this->configuration['index_as_default'] && $this->configuration['index_as_name'] ? $this->configuration['index_as_name'] : IndexBuilder::getIndexName($query->getIndex());
      $response_data = $backend->getClient()->request('indexes/' . $index_name . '/docs/autocomplete', 'POST', [
        'json' => $params,
      ]);

      if ($response_data->isSuccessful()) {
        $search_suggestions = $response_data->getBody()['value'];

        if (!empty($search_suggestions)) {
          $user_inputs = explode(' ', $user_input);
          $last_key = key(array_slice($user_inputs, -1, 1, TRUE));
          $user_inputs[$last_key] = '';
          $suggestion_factory = new SuggestionFactory(implode(' ', $user_inputs));

          foreach ($search_suggestions as $search_suggestion) {
            $suggestions[] = $suggestion_factory->createFromSuggestionSuffix($search_suggestion['text']);
          }
        }
      }
    }

    return $suggestions;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\search_api_autocomplete\SearchApiAutocompleteException
   */
  public static function supportsSearch(SearchInterface $search): ?AzureAiSearchBackendInterface {
    return static::getBackend($search->getIndex());
  }

}
