<?php

namespace Drupal\search_api_aais_logging\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for the tracking endpoints.
 */
class TrackingController extends ControllerBase {

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Active database connection.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(Connection $database, TimeInterface $time) {
    $this->database = $database;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('datetime.time')
    );
  }

  /**
   * Store tracking.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   *
   * @throws \Exception
   */
  public function storeTracking(Request $request): JsonResponse {
    $fields = [
      'uuid' => $request->get('uuid'),
      'uri' => $request->get('uri'),
      'position' => $request->get('position'),
      'timestamp' => $this->time->getRequestTime(),
    ];

    $query = $this->database->insert('search_api_aais_tracking')->fields($fields);
    $query->execute();

    return new JsonResponse(TRUE);
  }

  /**
   * Store semantic scoring.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   *
   * @throws \Exception
   */
  public function storeSemanticScoring(Request $request): JsonResponse {
    // We simply give a score of '1' on upvote, and '-1' on downvote.
    $expression = $request->get('action') === 'upvote' ? '1' : '-1';
    $query = $this->database->update('search_api_aais_logging')
      ->expression('semantic_scoring', $expression)
      ->condition('uuid', $request->get('uuid'));
    $query->execute();

    return new JsonResponse(TRUE);
  }

}
