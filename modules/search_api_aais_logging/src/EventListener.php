<?php

namespace Drupal\search_api_aais_logging;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Utility\Error;
use Drupal\search_api\Event\ProcessingResultsEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;
use Drupal\search_api_aais\Azure\Query\QueryParamBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener for processing the query results.
 */
class EventListener implements EventSubscriberInterface {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The query parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryParamBuilder
   */
  protected QueryParamBuilder $queryParamBuilder;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuidGenerator;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The event listener constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\search_api_aais\Azure\Query\QueryParamBuilder $query_param_builder
   *   The query parameter builder.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_generator
   *   The UUID generator.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Database\Connection $database
   *   Active database connection.
   */
  public function __construct(LoggerInterface $logger, QueryParamBuilder $query_param_builder, UuidInterface $uuid_generator, TimeInterface $time, Connection $database) {
    $this->logger = $logger;
    $this->queryParamBuilder = $query_param_builder;
    $this->uuidGenerator = $uuid_generator;
    $this->time = $time;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiEvents::PROCESSING_RESULTS => 'onProcessingResults',
    ];
  }

  /**
   * Reacts to the processing results event.
   *
   * @param \Drupal\search_api\Event\ProcessingResultsEvent $event
   *   The results alter event.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function onProcessingResults(ProcessingResultsEvent $event): void {
    // Get the result set and query.
    $result_set = $event->getResults();
    $result_query = $result_set->getQuery();

    // Get the index and its settings.
    $index = $result_query->getIndex();
    $index_settings = IndexBuilder::getIndexConfiguration($index);

    // Do not log any search requests if logging is not enabled for this index.
    if (!array_key_exists('search_api_aais_logging', $index_settings) || !$index_settings['search_api_aais_logging']['logging']) {
      return;
    }

    // Re-build the query parameters based on the original query ( e.g. before
    // any modifications ).
    $params = $this->queryParamBuilder->buildQueryParams($result_query->getOriginalQuery());

    // Do not log any 'blank' search requests.
    if (!isset($params['search'])) {
      return;
    }

    // Prepare and enter the data into the logging table.
    $fields = [
      'uuid' => $this->uuidGenerator->generate(),
      'index' => IndexBuilder::getIndexName($index),
      'search' => $params['search'],
      'filter' => $params['filter'] ?? '',
      'query_language' => $params['queryLanguage'] ?? '',
      'results' => $result_set->getResultCount(),
      'timestamp' => $this->time->getRequestTime(),
    ];

    // If a semantic answer was returned, log it as well.
    if ($semantic_answers = $result_set->getExtraData('search_aais.answers')) {
      $semantic_answer = reset($semantic_answers);

      // We will og the text value of the answer.
      $fields['semantic_answer'] = $semantic_answer['text'];
    }

    try {
      $this->database->insert('search_api_aais_logging')
        ->fields($fields)
        ->execute();
    }
    catch (\Exception $e) {
      // Log when for some reason we can't save the values.
      Error::logException($this->logger, $e);
    }

    // Set the generated UUID on the result set, so it can be used later on for
    // tracking if needed.
    $result_set->setExtraData('search_aais.uuid', $fields['uuid'] ?? NULL);
  }

}
