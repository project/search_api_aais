<?php

namespace Drupal\search_api_aais_logging\Plugin\views\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\search_api\Utility\Utility;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views field plugin for an entity URI.
 *
 * @ViewsField("entity_uri")
 */
class EntityUri extends FieldPluginBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The plugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['link_to_entity'] = ['default' => $this->definition['link_to_entity default'] ?? FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $form['link_to_entity'] = [
      '#title' => $this->t('Link this field to the original piece of content'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_entity']),
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);

    if ($value) {
      if ($this->options['link_to_entity']) {
        // Split the language property from the entity URI.
        [$uri, $language] = Utility::splitPropertyPath($value);

        try {
          // Generate an url to the entity, and fetch the route parameters and
          // entity type from the url.
          $entity_url = Url::fromUri($uri);

          $route_params = $entity_url->getRouteParameters();
          $entity_type = key($route_params);

          // Load the actual entity and its translation, based on the entity
          // type, route parameters and language.
          $entity = $this->entityTypeManager->getStorage($entity_type)
            ->load($route_params[$entity_type]);

          if ($entity instanceof ContentEntityInterface) {
            if ($language && $entity->isTranslatable() && $entity->hasTranslation($language)) {
              $entity = $entity->getTranslation($language);
            }
          }

          // Generate a link to the Entity.
          return Link::fromTextAndUrl($entity->label(), $entity_url)->toString();
        }
        catch (\Exception) {
          // Do not fail when the entity could not be converted to a link.
        }
      }
    }

    // If no url was returned, return a fallback renderable array.
    return [
      '#markup' => '-',
    ];

  }

}
