<?php

/**
 * @file
 * Provide views data for the Search API Azure AI Search module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function search_api_aais_logging_views_data(): array {
  $data = [];

  // Base table setup for the logging table.
  $data['search_api_aais_logging']['table']['group'] = t('Search API aais logging');
  $data['search_api_aais_logging']['table']['base'] = [
    'field' => 'id',
    'title' => t('Search API aais logging'),
    'weight' => -10,
  ];

  // Fields for the logging table.
  $data['search_api_aais_logging']['uuid'] = [
    'title' => t('UUID'),
    'help' => t('The unique identifier of the search.'),
    'field' => [
      'id' => 'standard',
    ],
    'relationship' => [
      'base' => 'search_api_aais_tracking',
      'base field' => 'uuid',
      'id' => 'standard',
      'label' => 'Search API aais tracking',
    ],
  ];

  $data['search_api_aais_logging']['index'] = [
    'title' => t('Index'),
    'help' => t('The index on which the search was performed.'),
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data['search_api_aais_logging']['search'] = [
    'title' => t('Search'),
    'help' => t('The text on which was searched.'),
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data['search_api_aais_logging']['filter'] = [
    'title' => t('Filter'),
    'help' => t('The filter used in the search.'),
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data['search_api_aais_logging']['query_language'] = [
    'title' => t('Query language'),
    'help' => t('The language on which was queried.'),
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data['search_api_aais_logging']['results'] = [
    'title' => t('Results'),
    'help' => t('The number of search results returned.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['search_api_aais_logging']['semantic_answer'] = [
    'title' => t('Semantic answer'),
    'help' => t('The semantic answer returned from the search.'),
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data['search_api_aais_logging']['semantic_scoring'] = [
    'title' => t('Semantic scoring'),
    'help' => t('The scoring of the semantic answer.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['search_api_aais_logging']['timestamp'] = [
    'title' => t('Timestamp'),
    'help' => t('The timestamp the search was performed.'),
    'field' => [
      'id' => 'date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
  ];

  // Base table setup for the tracking table.
  $data['search_api_aais_tracking']['table']['group'] = t('Search API aais tracking');
  $data['search_api_aais_tracking']['table']['base'] = [
    'field' => 'id',
    'title' => t('Search API aais tracking'),
    'weight' => -10,
  ];

  // Fields for the tracking table.
  $data['search_api_aais_tracking']['uuid'] = [
    'title' => t('Search UUID'),
    'help' => t('The unique identifier of the search.'),
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data['search_api_aais_tracking']['uri'] = [
    'title' => t('Entity URI'),
    'help' => t('The URI of the entity that was clicked.'),
    'field' => [
      'id' => 'entity_uri',
    ],
  ];

  $data['search_api_aais_tracking']['position'] = [
    'title' => t('Position'),
    'help' => t('The position the entity was in within the search results.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['search_api_aais_tracking']['timestamp'] = [
    'title' => t('Timestamp'),
    'help' => t('The timestamp the entity was clicked.'),
    'field' => [
      'id' => 'date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
  ];

  return $data;
}
