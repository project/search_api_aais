(function ($, Drupal) {
  /**
   * @type {{attach: Drupal.behaviors.initClickLogging.attach}}
   */
  Drupal.behaviors.initClickLogging = {
    attach(context, drupalSettings) {
      const $viewRows = $('[data-aais-logging-uri]');

      if ($viewRows.length) {
        const trackingSelector =
          drupalSettings.search_api_aais_logging.selector ?? 'a';

        $viewRows.each(function () {
          const $viewRow = $(this);
          const $anchors = $(trackingSelector, $viewRow);

          $anchors.once('initClickLogging').each(function () {
            const $anchor = $(this);

            $anchor.on('click', function () {
              // Fetch the Entity URI as well as the index from the View row.
              const entityUri = $viewRow.data('aais-logging-uri');
              const index = $viewRow.data('aais-logging-index');
              const uuid = drupalSettings.search_api_aais_logging.uuid;

              // If we have all the required parameters, register the tracking.
              // The 'index' parameter needs special care, as it can be 0
              // (= zero) which would fail a simple if check.
              if (
                entityUri &&
                uuid &&
                typeof index !== 'undefined' &&
                index !== false
              ) {
                const url = `${
                  drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix
                }js/search-api-aais-logging/store_tracking`;

                $.post({
                  url,
                  data: {
                    uuid: drupalSettings.search_api_aais_logging.uuid,
                    uri: entityUri,
                    position: index,
                  },
                });
              }
            });
          });
        });
      }
    },
  };

  /**
   * @type {{attach: Drupal.behaviors.initSemanticScoring.attach}}
   */
  Drupal.behaviors.initSemanticScoring = {
    attach(context, drupalSettings) {
      const uuid = drupalSettings.search_api_aais_logging.uuid;

      if (uuid) {
        const url = `${
          drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix
        }js/search-api-aais-logging/store_semantic_scoring`;

        const $voteButtons = $(
          '.js-aais-semantic-scoring-upvote, .js-aais-semantic-scoring-downvote',
        );
        const $upVoteButtons = $('.js-aais-semantic-scoring-upvote');
        const $downVoteButtons = $('.js-aais-semantic-scoring-downvote');

        if ($voteButtons.length) {
          // eslint-disable-next-line func-names
          $voteButtons.each(function () {
            const $voteButton = $(this);

            $voteButton
              .once('initSemanticScoring')
              .on('click', function (event) {
                const action = $voteButton.hasClass(
                  'js-aais-semantic-scoring-upvote',
                )
                  ? 'upvote'
                  : 'downvote';

                $.post({
                  url,
                  data: {
                    uuid,
                    action,
                  },
                  success: function success() {
                    // Disable the vote button, to prevent multiple subsequent clicks.
                    $voteButton.prop('disabled', true);

                    // If we 'upvoted', re-enable the downvote buttons and vice versa.
                    if (action === 'upvote') {
                      $downVoteButtons.prop('disabled', false);
                    } else {
                      $upVoteButtons.prop('disabled', false);
                    }
                  },
                });
              });
          });
        }
      }
    },
  };
})(jQuery, Drupal);
