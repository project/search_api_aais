<?php

/**
 * @file
 * Module file for Search API Azure AI Search.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;
use Drupal\search_api_aais\Azure\SynonymMap\SynonymMapBuilder;
use Drupal\search_api_aais\AzureAiSearchBackendInterface;

/**
 * Implements hook_theme().
 */
function search_api_aais_theme(): array {
  return [
    'semantic_answers' => [
      'variables' => [
        'answers' => [],
        'semantic_scoring' => FALSE,
        'search_results' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter() for the search_api_index_delete_form.
 */
function search_api_aais_form_search_api_index_delete_form_alter(&$form, FormStateInterface $form_state): void {
  /** @var \Drupal\search_api\Form\IndexDeleteConfirmForm $confirm_form */
  $confirm_form = $form_state->getFormObject();

  /** @var \Drupal\search_api\IndexInterface $index */
  $index = $confirm_form->getEntity();

  try {
    $search_api_server = $index->getServerInstance();
    if ($search_api_server === NULL) {
      throw new SearchApiException('The Search API server could not be loaded.');
    }
    // If we delete an Index from an Azure AI Search server, provide an
    // option to the User to delete the Index and/or the Synonym Map from the
    // remote Azure server.
    if (
      $search_api_server->hasValidBackend()
      && $search_api_server->getBackend()->getPluginId() === 'search_api_aais'
    ) {
      $form['delete_remote_index'] = [
        '#type' => 'checkbox',
        '#title' => t('Delete the Index from the remote Azure server'),
        '#description' => t('Check this to delete the Index from the remote Azure server.<br />Deleting the Index this way will automatically delete any associated Synonym Maps as well'),
      ];

      if ($index->getThirdPartySetting('search_api_aais', 'synonym_map')) {
        $form['delete_remote_synonymmap'] = [
          '#type' => 'checkbox',
          '#title' => t('Delete the Synonym Map from the remote Azure server'),
          '#description' => t('Check this to delete the Synonym Map from the remote Azure server.'),
          '#states' => [
            'visible' => [
              ':input[name="delete_remote_index"]' => ['checked' => FALSE],
            ],
          ],
        ];
      }

      // Provide an additional submit handler to deal with the Index and/or
      // Synonym Map deletion.
      $form['actions']['submit']['#submit'][] = 'search_api_aais_index_delete_submit';
    }
  }
  catch (SearchApiException $e) {
    \Drupal::logger('search_api_aais')->error(t('Error trying to get a Server Instance from Index @index : @error', [
      '@index' => $index->id(),
      '@error' => $e->getMessage(),
    ]));
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for the search_api_index_fields.
 */
function search_api_aais_form_search_api_index_fields_alter(&$form, FormStateInterface $form_state): void {
  /** @var \Drupal\search_api\Form\IndexFieldsForm $fields_form */
  $fields_form = $form_state->getFormObject();

  /** @var \Drupal\search_api\IndexInterface $index */
  $index = $fields_form->getEntity();

  try {
    $server = $index->getServerInstance();
    $backend = $server->getBackend();
  }
  catch (SearchApiException $e) {
    $server = $backend = NULL;
    \Drupal::logger('search_api_aais')->error(t('Error trying to get a Server Instance from Index @index : @error', [
      '@index' => $index->id(),
      '@error' => $e->getMessage(),
    ]));
  }

  if (!$server->hasValidBackend() || $backend->getPluginId() !== 'search_api_aais') {
    return;
  }

  $field_attributes = FieldsParamBuilder::getFieldAttributes();

  // Some fields are not stored in a specific datasource (e.g. entity:node), but
  // rather under the '_general' category.
  $datasources = $index->getDatasources();
  $datasources['_general'] = [];

  foreach ($datasources as $datasource_id => $datasource) {
    // Add the various field attributes to the table header.
    foreach ($field_attributes as $field_attribute_settings) {
      $form[$datasource_id]['#header'][] = $field_attribute_settings['label'];
    }

    // If the datasource does not have any fields defined, we can stop here.
    if (!isset($form[$datasource_id]) || !isset($form[$datasource_id]['fields'])) {
      continue;
    }

    // Get the field definitions for a given datasource, if applicable.
    $fields = ($datasource_id === '_general') ? $index->getFieldsByDatasource(NULL) : $index->getFieldsByDatasource($datasource_id);

    foreach ($fields as $field) {
      // Get the field configuration.
      $field_configuration = $field->getConfiguration() ?? [];

      // Prevent changing the field type on existing fields, as Azure doesn't
      // really like that.
      if (array_key_exists('searchable', $field_configuration)) {
        $form[$datasource_id]['fields'][$field->getFieldIdentifier()]['type']['#disabled'] = TRUE;
      }

      foreach ($field_attributes as $field_attribute => $field_attribute_settings) {
        // For each of the field attributes, provide a checkbox and fetch its
        // default value from the field configuration.
        $form[$datasource_id]['fields'][$field->getFieldIdentifier()][$field_attribute] = [
          '#type' => 'checkbox',
          '#default_value' => $field_configuration[$field_attribute] ?? NULL,
        ];

        // If a field attribute has multiple options, convert it the checkbox
        // to a dropdown list.
        if (isset($field_attribute_settings['options'])) {
          $form[$datasource_id]['fields'][$field->getFieldIdentifier()][$field_attribute]['#type'] = 'select';
          $form[$datasource_id]['fields'][$field->getFieldIdentifier()][$field_attribute]['#options'] = $field_attribute_settings['options'];
        }

        // Existing field attributes cannot be changed once configured, unless
        // the attribute is explicitly flagged as 'changeable'.
        if (array_key_exists('searchable', $field_configuration) && !$field_attribute_settings['changeable']) {
          $form[$datasource_id]['fields'][$field->getFieldIdentifier()][$field_attribute]['#disabled'] = TRUE;
        }
        // If an attribute is limited to specific field types, add various
        // states to enable/disable the attribute as needed.
        elseif (isset($field_attribute_settings['types'])) {
          $field_type_state_values = [];

          foreach ($field_attribute_settings['types'] as $field_type_value_allowable => $field_type_values) {
            $field_allowable_key = $field_type_value_allowable === 'allowed' ? 'value' : '!value';

            foreach ($field_type_values as $field_type_value) {
              $field_type_state_values[] = [$field_allowable_key => $field_type_value];
            }
          }

          $form[$datasource_id]['fields'][$field->getFieldIdentifier()][$field_attribute]['#states']['enabled'] = [
            ':input[name="fields[' . $field->getFieldIdentifier() . '][type]"]' => $field_type_state_values,
          ];
        }

        // Add the description of the attribute if available.
        if (isset($field_attribute_settings['description'])) {
          $form[$datasource_id]['fields'][$field->getFieldIdentifier()][$field_attribute]['#attributes']['title'] = $field_attribute_settings['description'];
        }
      }
    }
  }

  // Provide a validation handler to check for valid configurations.
  $form['actions']['submit']['#validate'][] = 'search_api_aais_index_fields_validate';

  // Provide an additional submit handler to save the field attributes.
  array_unshift($form['actions']['submit']['#submit'], 'search_api_aais_index_fields_submit');
}

/**
 * Implements hook_form_FORM_alter().
 */
function search_api_aais_form_search_api_index_form_alter(&$form, FormStateInterface $form_state, $form_id): void {
  if (in_array($form_id, ['search_api_index_form', 'search_api_index_edit_form'])) {
    // Make sure have at least 1 active Azure server before enabling any 'third
    // party settings' on the index.
    $search_api_server_storage = \Drupal::entityTypeManager()->getStorage('search_api_server');

    /** @var \Drupal\search_api\ServerInterface[] $search_api_servers */
    $search_api_servers = $search_api_server_storage->loadByProperties(['status' => TRUE]);
    $search_api_servers_aais_options = $search_api_servers_aais_semantic_options = [];
    $azure_ai_search_server_ids = [];
    foreach ($search_api_servers as $search_api_server) {
      if ($search_api_server->hasValidBackend() && $search_api_server->getBackend()->getPluginId() === 'search_api_aais') {
        $azure_ai_search_server_ids[] = $search_api_server->id();
        $search_api_servers_aais_options[] = ['value' => $search_api_server->id()];

        $backend_configuration = $search_api_server->getBackendConfig();

        if ($backend_configuration['connector_config'] && version_compare($backend_configuration['connector_config']['api_version'], AzureAiSearchBackendInterface::MINIMAL_SEMANTIC_CONFIGURATION_VERSION, '>=')) {
          $search_api_servers_aais_semantic_options[] = ['value' => $search_api_server->id()];
        }
      }
    }
    // Store a reference to all available servers in the form state. This will
    // allow us to conditionally validate input when an Azure Search API server
    // is selected.
    $form_state->set('azure_ai_search_server_ids', $azure_ai_search_server_ids);

    // Prepare the element for the specific settings for the Azure Search API
    // server.
    if (!empty($search_api_servers_aais_options)) {
      $configuration = [];
      /** @var \Drupal\Core\Entity\EntityFormInterface $index_form */
      $index_form = $form_state->getFormObject();
      /** @var \Drupal\search_api\IndexInterface $index */
      $index = $index_form->getEntity();

      if (!$index->isNew()) {
        $configuration = IndexBuilder::getIndexConfiguration($index);
      }

      // Only show the Azure AI Search Index settings if an active Azure server
      // was selected.
      $form['third_party_settings']['search_api_aais'] = [
        '#tree' => TRUE,
        '#type' => 'details',
        '#title' => t('Azure AI Search specific index options'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="server"]' => $search_api_servers_aais_options,
          ],
        ],
      ];

      $form['third_party_settings']['search_api_aais']['index_name'] = [
        '#type' => 'textfield',
        '#title' => t('Remote index name'),
        '#default_value' => $configuration['index_name'] ?? '',
        '#description' => [
          ['#markup' => t('The name of the index on Azure.')],
          ['#markup' => '<br />'],
          ['#markup' => t('The index name must only contain lowercase letters, digits or dashes, and cannot start or end with a dash.')],
        ],
        '#element_validate' => [
          'search_api_aais_form_search_api_index_form_validate_index_name',
        ],
        '#states' => [
          'required' => [
            ':input[name="server"]' => $search_api_servers_aais_options,
          ],
        ],
      ];

      // Add options on how to handle with pre-existing indexes.
      $form['third_party_settings']['search_api_aais']['skip_remote_index_actions'] = [
        '#type' => 'checkbox',
        '#title' => t('Skip remote index creation'),
        '#default_value' => $configuration['skip_remote_index_actions'] ?? 0,
        '#description' => [
          ['#markup' => t('If checked, this module will not attempt to create, update or delete the remote index. Use this in case the remote index already exists. You have to recreate the index field exactly the same as already defined in the remote index. The same applies to updates (e.g. new fields) to the index, they will not be propagated to the remote index.')],
        ],
      ];

      if ($index->isNew()) {
        $form['third_party_settings']['search_api_aais']['delete_remote_index'] = [
          '#type' => 'checkbox',
          '#title' => t('Delete pre-existing index'),
          '#default_value' => $configuration['delete_remote_index'] ?? 0,
          '#description' => [
            ['#markup' => '<strong>' . t('Warning: checking this option may cause data loss.') . '</strong>'],
            ['#markup' => '<br />'],
            ['#markup' => t('If checked, and a remote index with the same name already exists, it will be deleted and recreated empty. This allows to completely reset and add new fields to a remote index. Only check if you are completely sure as it is irreversible.')],
          ],
          '#states' => [
            'disabled' => [
              ':input[name="third_party_settings[search_api_aais][pre_existing_index_skip]"]' => ['checked' => TRUE],
            ],
          ],
        ];
      }
      // Prevent the remote index name to be changed after creation.
      else {
        $form['third_party_settings']['search_api_aais']['index_name']['#attributes'] = ['disabled' => 'disabled'];
      }

      $form['third_party_settings']['search_api_aais']['synonym_map'] = [
        '#type' => 'checkbox',
        '#title' => t('Use a Synonym Map'),
        '#default_value' => $configuration['synonym_map'] ?? 0,
        '#element_validate' => [
          'search_api_aais_form_search_api_index_form_validate_synonym_map',
        ],
      ];

      // Provide descriptive feedback to the User about the possible
      // implications of (un)checking the 'synonym_map'.
      if ($index->isNew() || !$configuration['synonym_map']) {
        $form['third_party_settings']['search_api_aais']['synonym_map']['#description'] = t("Check this option to automatically create a synonym map, based on the index name and the suffix '%suffix'.", [
          '%suffix' => SynonymMapBuilder::SUFFIX,
        ]);
      }
      else {
        $form['third_party_settings']['search_api_aais']['synonym_map']['#description'] = t('Uncheck this option <strong>will delete</strong> any associated synonym maps.');
      }

      $version_arg = [
        ':version' => AzureAiSearchBackendInterface::MINIMAL_SEMANTIC_CONFIGURATION_VERSION,
      ];
      $form['third_party_settings']['search_api_aais']['semantic_configuration'] = [
        '#type' => 'textfield',
        '#title' => t('Semantic configuration name'),
        '#default_value' => $configuration['semantic_configuration'] ?? '',
        '#disabled' => empty($search_api_servers_aais_semantic_options),
        '#states' => [
          'enabled' => [
            ':input[name="server"]' => $search_api_servers_aais_semantic_options,
          ],
          'required' => [
            ':input[name="server"]' => $search_api_servers_aais_semantic_options,
          ],
        ],
        '#description' => [
          ['#markup' => t('The name of the semantic configuration on Azure.')],
          ['#markup' => '<br />'],
          ['#markup' => t('Semantic configuration is <strong>required</strong> from version :version onwards to perform semantic searches.', $version_arg)],
        ],
      ];

      $form['third_party_settings']['search_api_aais']['captions'] = [
        '#type' => 'checkbox',
        '#title' => t('Search captions'),
        '#default_value' => $configuration['captions'] ?? 0,
        '#disabled' => empty($search_api_servers_aais_semantic_options),
        '#description' => [
          ['#markup' => t('When checked, the Azure captions will be used to generate the Search result excerpt.')],
          ['#markup' => '<br />'],
          ['#markup' => t('Only supported from API-version <strong>:version</strong> onwards.', $version_arg)],
        ],
      ];

      $form['third_party_settings']['search_api_aais']['highlight_tag'] = [
        '#type' => 'select',
        '#options' => [
          'em' => t('Emphasized text (@tag)', ['@tag' => '<em>']),
          'strong' => t('Strong text (@tag)', ['@tag' => '<strong>']),
          'mark' => t('Marked text (@tag)', ['@tag' => '<mark>']),
          'b' => t('Bold text (@tag)', ['@tag' => '<b>']),
        ],
        '#title' => t('Search captions highlight'),
        '#default_value' => $configuration['highlight_tag'] ?? 'em',
        '#states' => [
          'visible' => [
            ':input[name$="[search_api_aais][captions]"]' => ['checked' => TRUE],
          ],
        ],
        '#description' => t('The style to be used for highlighting the keywords in the search captions.'),
      ];

      if (\Drupal::moduleHandler()->moduleExists('search_api_aais_logging')) {
        $form['third_party_settings']['search_api_aais']['search_api_aais_logging'] = [
          '#tree' => TRUE,
          '#type' => 'details',
          '#title' => t('Logging & tracking'),
          '#open' => TRUE,
        ];

        $form['third_party_settings']['search_api_aais']['search_api_aais_logging']['logging'] = [
          '#type' => 'checkbox',
          '#title' => t('Enable logging'),
          '#description' => t('Log the search requests into the database. To enable click tracking, this must be enabled first.'),
          '#default_value' => $configuration['search_api_aais_logging']['logging'] ?? 0,
        ];

        $form['third_party_settings']['search_api_aais']['search_api_aais_logging']['tracking'] = [
          '#type' => 'checkbox',
          '#title' => t('Enable click tracking'),
          '#default_value' => $configuration['search_api_aais_logging']['tracking'] ?? 0,
          '#description' => t('Track the clicked search results into the database.'),
          '#states' => [
            'visible' => [
              ':input[name$="[search_api_aais_logging][logging]"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form['third_party_settings']['search_api_aais']['search_api_aais_logging']['tracking_selector'] = [
          '#type' => 'textfield',
          '#title' => t('Tracking selector'),
          '#default_value' => $configuration['search_api_aais_logging']['tracking_selector'] ?? 'a',
          '#description' => t('The jQuery selector for the clicks to track.'),
          '#field_prefix' => 'jQuery(\'',
          '#field_suffix' => '\');',
          '#states' => [
            'visible' => [
              ':input[name$="[search_api_aais_logging][logging]"]' => ['checked' => TRUE],
              ':input[name$="[search_api_aais_logging][tracking]"]' => ['checked' => TRUE],
            ],
            'required' => [
              ':input[name$="[search_api_aais_logging][tracking]"]' => ['checked' => TRUE],
            ],
          ],
        ];
      }

      // Allow other modules to add extra form elements as third party setting.
      $context = [
        'configuration' => $configuration,
        'index' => $index,
      ];
      \Drupal::moduleHandler()->alter('search_api_aais_index_settings_form', $form, $form_state, $context);
    }
  }
}

/**
 * Validation handler for the index name field on the index form.
 *
 * @param array $element
 *   The form element.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 * @param array $form
 *   The form.
 */
function search_api_aais_form_search_api_index_form_validate_index_name(array &$element, FormStateInterface $form_state, array $form): void {
  $server = $form_state->getValue('server');
  $azure_ai_search_server_ids = $form_state->get('azure_ai_search_server_ids');
  if (!$server || !$azure_ai_search_server_ids) {
    // If no server was selected or there are no Azure AI servers added, we
    // shouldn't validate the index name.
    return;
  }

  if (!in_array($server, $azure_ai_search_server_ids)) {
    // If the selected server is of not of the type "Azure AI Search", skip the
    // validation of the index name.
    return;
  }

  // Check if the desired remote index name adheres to the Azure specifications.
  if (!preg_match('/^[a-z0-9]+[a-z0-9-]*[a-z0-9]+$/', $element['#value'])) {
    $form_state->setError($element, t('The index name must be at least 2 characters long, only contain lowercase letters, digits or dashes, and cannot start or end with a dash.'));
  }
  else {
    /** @var \Drupal\Core\Entity\EntityFormInterface $index_form */
    $index_form = $form_state->getFormObject();
    /** @var \Drupal\search_api\IndexInterface $index */
    $index = $index_form->getEntity();

    // Check if the remote index name already exists. If we did not choose to
    // delete it, this will result in an error. If we chose to delete it, the
    // index will be removed before adding re-adding it.
    if ($index->isNew() && $index->getServerInstance()) {
      /** @var \Drupal\search_api_aais\Plugin\search_api\backend\SearchApiAzureAiSearchBackend $backend */
      $backend = $index->getServerInstance()->getBackend();
      if ($backend->getClient()->indexExists($index)) {
        $skip_index_creation = $index->getThirdPartySetting('search_api_aais', 'skip_remote_index_actions');
        // Only validate if the index exists when it should be (re-)created.
        if (!$skip_index_creation) {
          $delete_if_exists = $index->getThirdPartySetting('search_api_aais', 'delete_remote_index');
          if ($delete_if_exists) {
            try {
              $backend->getClient()->removeIndex($index);
            }
            catch (\Exception $e) {
              $form_state->setError($element, t('Something went wrong with deleting the remote index %index_name: %error.', [
                '%index_name' => $element['#value'],
                '%error' => $e->getMessage(),
              ]));
            }
          }
          else {
            // Show an error to the user when there is an already existing
            // remote index with the same name.
            $form_state->setError($element, t('There is already a remote index with the name %index_name.', [
              '%index_name' => $element['#value'],
            ]));
          }
        }
      }
    }
  }
}

/**
 * Validation handler for the synonym map on the index form.
 *
 * @param array $element
 *   The form element.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 * @param array $form
 *   The form.
 */
function search_api_aais_form_search_api_index_form_validate_synonym_map(array &$element, FormStateInterface $form_state, array $form): void {
  // Apparently, there is a limit to the number of synonym maps that can be
  // created on the remote index. So if a new synonym map is requested, check if
  // the maximum quota of synonym maps was not yet exceeded.
  if (!$element['#default_value'] && $element['#value']) {
    /** @var \Drupal\Core\Entity\EntityFormInterface $index_form */
    $index_form = $form_state->getFormObject();
    /** @var \Drupal\search_api\IndexInterface $index */
    $index = $index_form->getEntity();

    /** @var \Drupal\search_api_aais\Plugin\search_api\backend\SearchApiAzureAiSearchBackend $backend */
    $backend = $index->getServerInstance()->getBackend();

    $synonym_maps = $backend->getClient()->listSynonymMaps();

    if (count($synonym_maps) >= SynonymMapBuilder::LIMIT) {
      $form_state->setError($element, t('The synonym map quota of @count has been exceeded for this service. Please delete unused synonym maps first before creating a new one.', [
        '@count' => SynonymMapBuilder::LIMIT,
      ]));
    }
  }
}

/**
 * Submit handler for the index delete form of an Azure AI Search index.
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function search_api_aais_index_delete_submit(array $form, FormStateInterface $form_state): void {
  $values = $form_state->getValues();

  /** @var \Drupal\search_api\Form\IndexDeleteConfirmForm $confirm_form */
  $confirm_form = $form_state->getFormObject();

  /** @var \Drupal\search_api\IndexInterface $index */
  $index = $confirm_form->getEntity();

  /** @var \Drupal\search_api_aais\Plugin\search_api\backend\SearchApiAzureAiSearchBackend $backend */
  $backend = $index->getServerInstance()->getBackend();

  if ($values['delete_remote_index']) {
    $backend->getClient()->removeIndex($index);

    // An Index might not have an actual Synonym Map configured, so verify that
    // before trying to delete the Synonym Map.
    if (isset($values['delete_remote_synonymmap'])) {
      $backend->getClient()->deleteSynonymMap($index);
    }
  }
  elseif (isset($values['delete_remote_synonymmap'])) {
    $backend->getClient()->deleteSynonymMap($index);
  }
}

/**
 * Submit handler for the index fields form of an Azure AI Search index.
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function search_api_aais_index_fields_validate(array &$form, FormStateInterface $form_state): void {
  $field_values = $form_state->getValue('fields', []);

  foreach ($field_values as $field_id => $field_settings) {
    if (isset($field_settings['analyzer']) && $field_settings['analyzer'] !== '' && $field_settings['analyzer'] !== '_none') {
      // An analyzer cannot be set on a field that is not deemed 'searchable'.
      if (!$field_settings['searchable']) {
        $form_state->setErrorByName('fields][' . $field_id . '][analyzer', t('An analyzer can only be configured on a searchable field. Either configure the field %field as searchable, or disable the analyzer.', [
          '%field' => $field_id,
        ]));
        $form_state->setErrorByName('fields][' . $field_id . '][searchable', t('An analyzer can only be configured on a searchable field. Either configure the field %field as searchable, or disable the analyzer.', [
          '%field' => $field_id,
        ]));
      }

      // A field can only be added to the source fields of the suggester, if it
      // uses a language analyzer, or no analyzer at all.
      if ($field_settings['suggester'] && !in_array($field_settings['analyzer'], array_keys(FieldsParamBuilder::getLanguageAnalyzers()))) {
        $form_state->setErrorByName('fields][' . $field_id . '][analyzer', t('An field can only be added to the suggester if it uses a language analyzer, or no analyzer at all. Either remove the field %field from the suggester, or choose a compatible analyzer.', [
          '%field' => $field_id,
        ]));
        $form_state->setErrorByName('fields][' . $field_id . '][suggester', t('An field can only be added to the suggester if it uses a language analyzer, or no analyzer at all. Either remove the field %field from the suggester, or choose a compatible analyzer.', [
          '%field' => $field_id,
        ]));
      }
    }
  }
}

/**
 * Submit handler for the index fields form of an Azure AI Search index.
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function search_api_aais_index_fields_submit(array &$form, FormStateInterface $form_state): void {
  /** @var \Drupal\search_api\Form\IndexFieldsForm $fields_form */
  $fields_form = $form_state->getFormObject();

  /** @var \Drupal\search_api\IndexInterface $index */
  $index = $fields_form->getEntity();

  $field_attributes = FieldsParamBuilder::getFieldAttributes();
  $field_values = $form_state->getValue('fields', []);

  $fields = $index->getFields(TRUE);

  $new_fields = [];

  foreach ($field_values as $field_id => $field_settings) {
    $field = $fields[$field_id];

    // Filter out any '_none' values.
    $field_settings = array_filter($field_settings, function ($value) {
      return $value !== '_none';
    });

    // Get the existing field configuration.
    $field_configuration = $field->getConfiguration();

    // Merge the Azure-specific field attributes into the field configuration
    // and save it.
    $updated_configuration = array_intersect_key($field_settings, $field_attributes);
    foreach ($updated_configuration as $config_key => $config_value) {
      $field_configuration[$config_key] = $config_value;
    }

    $field->setConfiguration($field_configuration);

    $new_fields[$field_settings['id']] = $field;
  }

  $index->setFields($new_fields);
}

/**
 * Implements hook_views_data().
 */
function search_api_aais_views_data(): array {
  $data['views']['semantic_answer'] = [
    'title' => t('Semantic answer'),
    'help' => t('Shows the Azure semantic answer.'),
    'area' => [
      'id' => 'semantic_answer',
    ],
  ];

  return $data;
}
