<?php

namespace Drupal\search_api_aais\Connector;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;
use Drupal\search_api\LoggerTrait;
use Drupal\search_api\Plugin\ConfigurablePluginBase;
use Drupal\search_api_aais\BackendClient\BackendClientFactory;
use Drupal\search_api_aais\BackendClient\BackendClientInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Defines a base class for connector plugins.
 */
abstract class AzureAiSearchConnectorPluginBase extends ConfigurablePluginBase implements PluginFormInterface {

  use LoggerTrait;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The Azure AI Search API client factory.
   *
   * @var \Drupal\search_api_aais\BackendClient\BackendClientFactory
   */
  protected BackendClientFactory $backendClientFactory;

  /**
   * Sets the event dispatcher.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function setEventDispatcher(EventDispatcherInterface $event_dispatcher): void {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Sets the backend client factory.
   *
   * @param \Drupal\search_api_aais\BackendClient\BackendClientFactory $backend_client_factory
   *   The backend client factory.
   */
  public function setBackendClientFactory(BackendClientFactory $backend_client_factory): void {
    $this->backendClientFactory = $backend_client_factory;
  }

  /**
   * Gets the API client.
   *
   * @return \Drupal\search_api_aais\BackendClient\BackendClientInterface
   *   The API client.
   */
  protected function createClient(array &$configuration): BackendClientInterface {
    return $this->backendClientFactory->create($this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'url' => '',
      'api_version' => '2020-06-30',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['url'] = [
      '#title' => $this->t('Url'),
      '#description' => $this->t('The URL to your Azure AI Search server.'),
      '#type' => 'url',
      '#required' => TRUE,
      '#default_value' => $this->configuration['url'] ?? '',
    ];

    $form['api_version'] = [
      '#title' => $this->t('API-version'),
      '#type' => 'select',
      '#options' => [
        '2020-06-30' => $this->t('2020-06-30 (Stable)'),
        '2020-06-30-Preview' => $this->t('2020-06-30 (Preview)'),
        '2021-04-30-Preview' => $this->t('2021-04-30 (Preview)'),
        '2023-10-01-preview' => $this->t('2023-10-01 (Preview)'),
        '2023-11-01' => $this->t('2023-11-01 (Stable)'),
        '2024-03-01-preview' => $this->t('2024-03-01 (Preview)'),
        '2024-05-01-preview' => $this->t('2024-05-01 (Preview)'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['api_version'] ?? '2020-06-30',
      '#description' => $this->t('See <a href="@api-versions" target="_blank">the documentation</a> for the complete list of API-versions.', [
        '@api-versions' => Url::fromUri('https://docs.microsoft.com/en-us/rest/api/searchservice/search-service-api-versions')->toString(),
      ]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $url = $form_state->getValue('url');
    if (!UrlHelper::isValid($url)) {
      $form_state->setErrorByName('url', $this->t('The URL is invalid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['url'] = rtrim($form_state->getValue('url'), '/');
    $this->configuration['api_version'] = $form_state->getValue('api_version');
  }

  /**
   * Get the URL value.
   *
   * @return string
   *   The URL.
   */
  public function getUrl(): string {
    return (string) $this->configuration['url'];
  }

  /**
   * Get the API version value.
   *
   * @return string
   *   The API version.
   */
  public function getApiVersion(): string {
    return (string) $this->configuration['api_version'];
  }

}
