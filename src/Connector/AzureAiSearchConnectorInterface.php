<?php

namespace Drupal\search_api_aais\Connector;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api_aais\BackendClient\BackendClientFactory;
use Drupal\search_api_aais\BackendClient\BackendClientInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Interface for Azure AI Search Connector plugins.
 */
interface AzureAiSearchConnectorInterface extends PluginFormInterface, ConfigurableInterface, PluginInspectionInterface {

  /**
   * Sets the event dispatcher.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function setEventDispatcher(EventDispatcherInterface $event_dispatcher);

  /**
   * Sets the backend client factory.
   *
   * @param \Drupal\search_api_aais\BackendClient\BackendClientFactory $backend_client_factory
   *   The client factory.
   */
  public function setBackendClientFactory(BackendClientFactory $backend_client_factory);

  /**
   * Gets the API client.
   *
   * @return \Drupal\search_api_aais\BackendClient\BackendClientInterface
   *   The API client.
   */
  public function createClient(array &$configuration): BackendClientInterface;

  /**
   * Gets the url to the Azure AI Search API.
   *
   * @return string
   *   The Azure AI Search API.
   */
  public function getUrl(): string;

  /**
   * Gets the version of the API.
   *
   * @return string
   *   The API version.
   */
  public function getApiVersion(): string;

}
