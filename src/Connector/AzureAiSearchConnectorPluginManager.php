<?php

namespace Drupal\search_api_aais\Connector;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\search_api_aais\Annotation\AzureAiSearchConnector;

/**
 * A plugin manager for Azure AI Search connector plugins.
 *
 * @see \Drupal\search_api_aais\Annotation\AzureAiSearchConnector
 * @see \Drupal\search_api_aais\Connector\AzureAiSearchConnectorInterface
 *
 * @ingroup plugin_api
 */
class AzureAiSearchConnectorPluginManager extends DefaultPluginManager {

  /**
   * Constructs a ConnectorPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $this->alterInfo('aais_connector_info');
    $this->setCacheBackend($cache_backend, 'aais_connector_plugins');

    parent::__construct('Plugin/AzureAiSearch/Connector', $namespaces, $module_handler, AzureAiSearchConnectorInterface::class, AzureAiSearchConnector::class);
  }

}
