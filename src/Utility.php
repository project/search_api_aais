<?php

namespace Drupal\search_api_aais;

/**
 * Provides helper methods when handling with search results.
 */
class Utility {

  /**
   * Convert an Azure index item ID to an entity URI.
   *
   * Search API uses the entity URI as the index item ID. However, Azure does
   * not allow colons or slashes in the index item ID, so before indexing the
   * item ID's special characters are converted.
   * This function reconstructs the original entity URI, from a given Azure
   * item ID.
   *
   * @param string $item_id
   *   The Azure index item ID.
   *   Structured as <datasource>_<entity_type>_<entity_id>_<langcode>.
   * @param bool $include_langcode
   *   (optional) Whether to the language code in the entity URI. Make sure this
   *   is FALSE when using the entity URI in routing. Defaults to FALSE.
   *
   * @return string
   *   The entity URI.
   */
  public static function itemIdToEntityUri(string $item_id, bool $include_langcode = FALSE): string {
    $parts = explode('_', $item_id);
    $parts_count = count($parts);

    $datasource = $parts[0];
    $entity_id = $parts[$parts_count - 2];
    $langcode = $parts[$parts_count - 1];

    // By exclusion of other parts, the remainder of the item
    // ID should be the entity type (e.g. node, taxonomy_term, ..).
    unset($parts[0], $parts[$parts_count - 2], $parts[$parts_count - 1]);
    $entity_type_id = implode('_', $parts);

    // Reconstruct the drupal entity uri.
    $parts = [$datasource, implode('/', [$entity_type_id, $entity_id])];

    // Include the langcode if specifically requested.
    if ($include_langcode) {
      $parts[] = $langcode;
    }

    return implode(':', $parts);
  }

}
