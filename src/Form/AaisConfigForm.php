<?php

namespace Drupal\search_api_aais\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Azure AI Search settings.
 */
class AaisConfigForm extends ConfigFormBase {

  const SETTINGS = 'search_api_aais.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'search_api_aais_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::SETTINGS);

    $form['log_queries'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log queries'),
      '#default_value' => $config->get('log_queries') ?? FALSE,
      '#description' => $this->t('Logs the queries that are sent to Azure into the site logs. Use for debugging issues with the API connection.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->cleanValues();

    $this
      ->config(self::SETTINGS)
      ->setData($form_state->getValues())
      ->save();

    parent::submitForm($form, $form_state);
  }

}
