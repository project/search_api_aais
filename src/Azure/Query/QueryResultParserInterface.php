<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api_aais\BackendClient\ResponseData;

/**
 * Interface for the query result parser.
 */
interface QueryResultParserInterface {

  /**
   * Parse an Azure API response to add data to it.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The query.
   * @param \Drupal\search_api_aais\BackendClient\ResponseData $response
   *   Raw response from Azure.
   */
  public function parseResult(QueryInterface $query, ResponseData $response): void;

}
