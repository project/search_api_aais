<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Utility\FieldsHelperInterface;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;

/**
 * Query parameter builder.
 */
class QueryParamBuilder implements QueryParamBuilderInterface {

  /**
   * The query filter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryFilterBuilder
   */
  protected QueryFilterBuilder $queryFilterBuilder;

  /**
   * The query sort builder.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QuerySortBuilder
   */
  protected QuerySortBuilder $querySortBuilder;

  /**
   * The query facet param builder.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryFacetParamBuilder
   */
  protected QueryFacetParamBuilder $queryFacetParamBuilder;

  /**
   * The Search API fields helper.
   *
   * @var \Drupal\search_api\Utility\FieldsHelperInterface
   */
  protected FieldsHelperInterface $fieldsHelper;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The constructor.
   *
   * @param \Drupal\search_api_aais\Azure\Query\QueryFilterBuilder $query_filter_builder
   *   The query filter builder.
   * @param \Drupal\search_api_aais\Azure\Query\QuerySortBuilder $query_sort_builder
   *   The query sort builder.
   * @param \Drupal\search_api_aais\Azure\Query\QueryFacetParamBuilder $query_facet_param_builder
   *   The query facet param builder.
   * @param \Drupal\search_api\Utility\FieldsHelperInterface $fields_helper
   *   The Search API fields helper.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(QueryFilterBuilder $query_filter_builder, QuerySortBuilder $query_sort_builder, QueryFacetParamBuilder $query_facet_param_builder, FieldsHelperInterface $fields_helper, LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {
    $this->queryFilterBuilder = $query_filter_builder;
    $this->querySortBuilder = $query_sort_builder;
    $this->queryFacetParamBuilder = $query_facet_param_builder;
    $this->fieldsHelper = $fields_helper;
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function buildQueryParams(QueryInterface $query): array {
    $index = $query->getIndex();
    $index_fields = $this->getIndexFields($index);

    /** @var \Drupal\search_api_aais\AzureAiSearchBackendInterface $backend */
    $backend = $index->getServerInstance()->getBackend();

    // Set-up some default parameters for a semantic search.
    $params = [
      // (false)|true.
      'count' => TRUE,
      // (any)|all.
      'searchMode' => 'any',
      // (*)|..
      'select' => 'itemId',
      // (simple)|full|semantic
      // Currently this module ha sno support for the full query type or the
      // full lucene syntax. If you should need this, use an alter hook to alter
      // the query and changes the params you need.
      'queryType' => 'simple',
      // (50)|0..1000
      'top' => $query->getOption('limit', 10),
      // (0)|0..100000
      'skip' => $query->getOption('offset', 0),
    ];

    // Some query parameters and/or values are only available when using the
    // 'preview' version of the API.
    // @see https://learn.microsoft.com/en-us/rest/api/searchservice/preview-api/search-documents#query-parameters
    if ($backend->inPreviewMode()) {
      // (simple)|full|semantic
      $params['queryType'] = 'semantic';
      // (none)|extractive
      $params['answers'] = 'extractive';
      // (none)|lexicon
      $params['speller'] = 'lexicon';

      // (disabled)|enabled
      $params['featuresMode'] = 'enabled';

      // Check for any language filter(s) that needs to be applied.
      if ($language_filters = $query->getLanguages()) {
        // If we need to filter on one or more languages, add it to the
        // conditions so that it will be picked up by the query filter builder.
        // @see \Drupal\search_api_aais\Azure\Query\QueryFilterBuilder
        $query->getConditionGroup()->addCondition('search_api_language', $language_filters, 'IN');

        // Set the query language based on the language filters.
        // However, Azure only accepts a single language as a filter, so we just
        // take the first one.
        $query_language = reset($language_filters);
      }
      else {
        // If there are no language filters, set the query language based on the
        // current language.
        $query_language = $this->languageManager->getCurrentLanguage()->getId();
      }

      // Set the query language.
      $params['queryLanguage'] = $query_language;
    }

    // If the configured API-version in the backend requires semantic
    // configuration, pass it on.
    if ($backend->requiresSemanticConfiguration()) {
      $params['semanticConfiguration'] = IndexBuilder::getIndexConfiguration($index, 'semantic_configuration');
    }

    // Set all full-text fields as the fields to actually search in.
    $params['searchFields'] = implode(', ', $index->getFulltextFields());

    // Construct the filters.
    $filters = $this->queryFilterBuilder->buildQueryFilters($query->getConditionGroup(), IndexBuilder::getIndexFields($index));

    if (!empty($filters)) {
      // @todo Improve the hardcoded 'and' concatenation.
      $params['filter'] = implode(' and ', $filters);
    }

    // Construct the sorts.
    $sorts = $this->querySortBuilder->buildQuerySorts($query->getSorts(), IndexBuilder::getIndexFields($index));

    if (!empty($sorts)) {
      $params['orderby'] = implode(', ', $sorts);

      // Sorting cannot be applied if the queryType is 'semantic', as Azure then
      // automatically sorts the results based on their relevance.
      // So if any sorting is present on a 'semantic' queryType, switch to the
      // 'full' queryType instead and change the parameters as needed.
      if ($params['queryType'] === 'semantic') {
        $params['queryType'] = 'full';
        unset($params['answers']);
        unset($params['semanticConfiguration']);
      }
    }

    // If the captions option is enabled and the queryType is 'semantic', apply
    // the configured highlight tags.
    if ($params['queryType'] === 'semantic' && IndexBuilder::getIndexConfiguration($index, 'captions')) {
      $tag = IndexBuilder::getIndexConfiguration($index, 'highlight_tag') ?? 'em';

      $params['captions'] = 'extractive';
      $params['highlightPreTag'] = sprintf('<%s>', $tag);
      $params['highlightPostTag'] = sprintf('</%s>', $tag);
    }

    // Set the search keys as the actual search query.
    if ($keys = $query->getKeys()) {
      $keys = array_filter($keys, function (string $key) {
        return $key[0] !== '#';
      }, ARRAY_FILTER_USE_KEY);

      $params['search'] = implode(' ', $keys);
    }

    if (!empty($query->getOption('search_api_facets'))) {
      $facet_params = $this->queryFacetParamBuilder->buildFacetParams($query, $index_fields);
      if (!empty($facet_params)) {
        $params['facets'] = $facet_params;
      }
    }

    // Allow other modules to alter the query parameters.
    $this->moduleHandler->alter('search_api_aais_query_params', $query, $params);

    return $params;
  }

  /**
   * Gets the list of index fields.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index.
   *
   * @return \Drupal\search_api\Item\FieldInterface[]
   *   The index fields, keyed by field identifier.
   */
  protected function getIndexFields(IndexInterface $index): array {
    $index_fields = $index->getFields();

    // Search API does not provide metadata for some special fields but might
    // try to query for them. Thus add the metadata so we allow for querying
    // them.
    if (empty($index_fields['search_api_datasource'])) {
      $index_fields['search_api_datasource'] = $this->fieldsHelper->createField($index, 'search_api_datasource', ['type' => 'string']);
    }
    if (empty($index_fields['search_api_id'])) {
      $index_fields['search_api_id'] = $this->fieldsHelper->createField($index, 'search_api_id', ['type' => 'string']);
    }
    return $index_fields;
  }

}
