<?php

namespace Drupal\search_api_aais\Azure\Query;

/**
 * Builder for the query sorts.
 */
class QuerySortBuilder implements QuerySortBuilderInterface {

  /**
   * Builds a list of sorts.
   *
   * @param array $sorts
   *   An array of sort instructions.
   * @param \Drupal\search_api\Item\Field[] $fields
   *   The fields available on the Index.
   *
   * @return array
   *   List of sorts
   */
  public function buildQuerySorts(array $sorts, array $fields): array {
    $sorters = [];

    foreach ($sorts as $field_name => $order) {
      // Make sure the field exists on the index, or is one of the Search API
      // specific fields.
      if (!isset($fields[$field_name])) {
        continue;
      }

      $field_configuration = $fields[$field_name]->getConfiguration();

      // In addition, the field has to be configured as 'sortable'.
      if (!$field_configuration['sortable']) {
        continue;
      }

      $sorters[] = sprintf('%s %s', ($field_name === 'search_api_relevance') ? 'search.score()' : $field_name, strtolower($order));
    }

    return $sorters;
  }

}
