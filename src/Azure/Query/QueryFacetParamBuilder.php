<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\search_api\Query\QueryInterface;
use Psr\Log\LoggerInterface;

/**
 * Builds the facet params.
 */
class QueryFacetParamBuilder {

  /**
   * Creates a new Facet builder.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    protected LoggerInterface $logger,
  ) {
  }

  /**
   * Fill the facets array of the request.
   *
   * The facets need to be converted to type string[].
   * The list of facet expressions to apply to the search query. Each facet
   * expression contains a field name, optionally followed by a comma-separated
   * list of name:value pairs. Currently only the first scenario is supported.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   Search API query.
   * @param array $index_fields
   *   The index field, keyed by field identifier.
   *
   * @return array
   *   The facets params.
   */
  public function buildFacetParams(QueryInterface $query, array $index_fields) {
    $params = [];
    $facets = $query->getOption('search_api_facets', []);
    if (empty($facets)) {
      return $params;
    }

    foreach ($facets as $facet_id => $facet) {
      $field = $facet['field'];
      if (!isset($index_fields[$field])) {
        $this->logger->warning('Unknown facet field: %field', ['%field' => $field]);
        continue;
      }
      $params[] = $facet_id;
    }

    return $params;
  }

}
