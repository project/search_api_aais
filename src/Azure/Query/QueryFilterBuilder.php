<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\search_api\Query\Condition;
use Drupal\search_api\Query\ConditionGroupInterface;

/**
 * Builds the query filters.
 */
class QueryFilterBuilder implements QueryFilterBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function buildQueryFilters(ConditionGroupInterface $condition_group, array $fields): array {
    $filters = [];

    $conditions = $condition_group->getConditions();

    foreach ($conditions as $condition) {
      if ($condition instanceof Condition) {
        if (!isset($fields[$condition->getField()])) {
          continue;
        }

        $field = $fields[$condition->getField()];

        // Convert the filters to an Azure API filter based on the field type.
        // This could be improved.
        switch ($condition->getOperator()) {
          case 'IN':
            switch ($field->getType()) {
              case 'string':
                $filters[] = sprintf("search.in(%s, '%s')", $condition->getField(), implode(', ', $condition->getValue()));
                break;

              case 'aais.integer_collection':
                $condition_value = $condition->getValue();
                $condition_value = (int) array_pop($condition_value);

                $filters[] = sprintf('%s/any(t: t eq %d)', $condition->getField(), $condition_value);
                break;
            }
            break;

          default:
            // Basic support for general filter queries. Extends where needed.
            $operator = match ($condition->getOperator()) {
              '!=' => 'ne',
              '>' => 'gt',
              '>=' => 'ge',
              '<' => 'lt',
              '<=' => 'le',
              // Matches '='.
              default => 'eq',
            };

            $filters[] = match ($field->getType()) {
              'boolean' => sprintf('%s %s %s', $condition->getField(), $operator, $condition->getValue() ? 'true' : 'false'),
              'integer', 'duration' => sprintf("%s %s %d", $condition->getField(), $operator, $condition->getValue()),
              'decimal' => sprintf("%s %s %f", $condition->getField(), $operator, $condition->getValue()),
              default => sprintf("%s %s '%s'", $condition->getField(), $operator, $condition->getValue()),
            };

            break;
        }
      }
      elseif ($condition instanceof ConditionGroupInterface) {
        $nested_filters = $this->buildQueryFilters($condition, $fields);

        if (!empty($nested_filters)) {
          $filters = array_merge($filters, $nested_filters);
        }
      }
    }

    return $filters;
  }

}
