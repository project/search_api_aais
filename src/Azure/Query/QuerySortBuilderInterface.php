<?php

namespace Drupal\search_api_aais\Azure\Query;

/**
 * Interface for the query sorts builder.
 */
interface QuerySortBuilderInterface {

  /**
   * Builds a list of sorts.
   *
   * @param array $sorts
   *   An array of sort instructions.
   * @param \Drupal\search_api\Item\Field[] $fields
   *   The fields available on the Index.
   *
   * @return array
   *   List of sorts
   */
  public function buildQuerySorts(array $sorts, array $fields): array;

}
