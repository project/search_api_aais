<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\search_api\Query\QueryInterface;

/**
 * Interface for the query parameter builder.
 */
interface QueryParamBuilderInterface {

  /**
   * Prepare a search request for Azure.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The Search API query.
   *
   * @return array
   *   An array of parameters for the search query.
   *
   * @throws \Drupal\search_api\SearchApiException
   *
   * @see https://docs.microsoft.com/en-us/rest/api/searchservice/preview-api/search-documents
   * @see https://learn.microsoft.com/en-us/rest/api/searchservice/search-service-api-versions
   */
  public function buildQueryParams(QueryInterface $query): array;

}
