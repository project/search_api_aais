<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\search_api\Query\ConditionGroupInterface;

/**
 * Interface for the QueryFilterBuilder.
 */
interface QueryFilterBuilderInterface {

  /**
   * Builds a list of filters for a condition group.
   *
   * @param \Drupal\search_api\Query\ConditionGroupInterface $condition_group
   *   The condition group for which to build the filters.
   * @param \Drupal\search_api\Item\FieldInterface[] $fields
   *   The fields available on the Index.
   *
   * @return array
   *   List of filters applicable to the condition group.
   */
  public function buildQueryFilters(ConditionGroupInterface $condition_group, array $fields): array;

}
