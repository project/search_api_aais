<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Utility\FieldsHelperInterface;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;
use Drupal\search_api_aais\BackendClient\ResponseData;

/**
 * Parses the query result to add data to the response object.
 */
class QueryResultParser implements QueryResultParserInterface {

  /**
   * The Search API fields helper.
   *
   * @var \Drupal\search_api\Utility\FieldsHelperInterface
   */
  protected FieldsHelperInterface $fieldsHelper;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The facet query result parser.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryFacetResultParser
   */
  protected QueryFacetResultParser $queryFacetResultParser;

  /**
   * The constructor.
   *
   * @param \Drupal\search_api\Utility\FieldsHelperInterface $fields_helper
   *   The field helper.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\search_api_aais\Azure\Query\QueryFacetResultParser $query_facet_result_parser
   *   The facet result parser.
   */
  public function __construct(FieldsHelperInterface $fields_helper, ModuleHandlerInterface $module_handler, QueryFacetResultParser $query_facet_result_parser) {
    $this->fieldsHelper = $fields_helper;
    $this->moduleHandler = $module_handler;
    $this->queryFacetResultParser = $query_facet_result_parser;
  }

  /**
   * {@inheritdoc}
   */
  public function parseResult(QueryInterface $query, ResponseData $response): void {
    // Fetch the results from the response.
    $response_results = $response->getBody();

    // Prepare the result set.
    $results = $query->getResults();

    $results->setResultCount($response_results['@odata.count']);

    if ($results->getResultCount()) {
      $use_captions = IndexBuilder::getIndexConfiguration($query->getIndex(), 'captions') ?? FALSE;

      foreach ($response_results['value'] as $response_result) {
        $result_item = $this->fieldsHelper->createItem($query->getIndex(), $response_result['itemId']);
        $result_item->setScore($response_result['@search.score']);

        // If the captions option is enabled, and the result actually contains
        // captions, replace the excerpt with the captions.
        if ($use_captions && array_key_exists('@search.captions', $response_result) && $response_result['@search.captions']) {
          $captions = end($response_result['@search.captions']);

          $result_item->setExcerpt(!empty($captions['highlights']) ? $captions['highlights'] : $captions['text']);
        }

        $results->addResultItem($result_item);
      }

      if (!empty($response_results['@search.facets'])) {
        $facets = $this->queryFacetResultParser->parseFacetResult($query, $response_results);
        $results->setExtraData('search_api_facets', $facets);
      }
    }

    $results->setExtraData('search_aais.answers', $response_results['@search.answers'] ?? []);

    // Allow other modules to add extra data to the results object.
    $this->moduleHandler->alter('search_api_aais_query_results', $query, $response);
  }

}
