<?php

namespace Drupal\search_api_aais\Azure\Query;

use Drupal\search_api\Query\QueryInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides a facet result parser.
 */
class QueryFacetResultParser {

  /**
   * Creates a new facet result parser.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    protected LoggerInterface $logger,
  ) {
  }

  /**
   * Parse the facet result.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The query.
   * @param array $response_results
   *   The response.
   *
   * @return array
   *   The facet data in the format expected by facets module.
   */
  public function parseFacetResult(QueryInterface $query, array $response_results): array {
    $facet_data = [];
    $facets = $query->getOption('search_api_facets', []);

    foreach ($facets as $facet_id => $facet) {
      $terms = [];
      if (isset($response_results['@search.facets'][$facet_id])) {
        foreach ($response_results['@search.facets'][$facet_id] as $facet_item) {
          $terms[] = [
            'count' => $facet_item['count'],
            'filter' => '"' . $facet_item['value'] . '"',
          ];
        }
      }
      $facet_data[$facet_id] = $terms;
    }
    return $facet_data;
  }

}
