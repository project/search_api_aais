<?php

namespace Drupal\search_api_aais\Azure\Documents;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\search_api\DataType\DataTypePluginManager;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValue;
use Drupal\search_api\Utility\FieldsHelperInterface;
use Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder;
use Psr\Log\LoggerInterface;

/**
 * The document parameter builder.
 */
class DocumentsParamBuilder {

  use StringTranslationTrait;

  /**
   * The Data Type plugin manager.
   *
   * @var \Drupal\search_api\DataType\DataTypePluginManager
   */
  protected DataTypePluginManager $dataTypePluginManager;

  /**
   * The field helper.
   *
   * @var \Drupal\search_api\Utility\FieldsHelperInterface
   */
  protected FieldsHelperInterface $fieldsHelper;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a DocumentsParamBuilder object.
   *
   * @param \Drupal\search_api\DataType\DataTypePluginManager $data_type_plugin_manager
   *   The data type plugin manager.
   * @param \Drupal\search_api\Utility\FieldsHelperInterface $fields_helper
   *   The field helper.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger instance.
   */
  public function __construct(DataTypePluginManager $data_type_plugin_manager, FieldsHelperInterface $fields_helper, LoggerInterface $logger) {
    $this->dataTypePluginManager = $data_type_plugin_manager;
    $this->fieldsHelper = $fields_helper;
    $this->logger = $logger;
  }

  /**
   * Creates a field value to add to the document.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The Search API field.
   *
   * @return mixed
   *   The field value to use for this field. Depending on the field types, this
   *   can be an array, string, integer, ...
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createFieldValues(FieldInterface $field) {
    $field_values = $field->getValues();

    // When the field contains no values at all, we can immediately stop and
    // simply return NULL.
    if (empty($field_values)) {
      return NULL;
    }

    // The field values are always an array, even if it just contains a single
    // value. However, Azure only accepts an array of values if the data type is
    // an actual collection.
    // So we just shift the field values except for Azure-specific data types
    // that are flagged as being a collection.
    if (in_array($field->getType(), FieldsParamBuilder::AZURE_DATA_TYPES)) {
      $plugin = $this->dataTypePluginManager->getDefinition($field->getType());

      // Check if the data type is a collection.
      // @see \Drupal\search_api_aais\Plugin\search_api\data_type\AzureDataTypeInterface::isCollection();
      $field_values = !call_user_func([$plugin['class'], 'isCollection']) ? end($field_values) : $field_values;
    }
    else {
      $field_values = is_array($field_values) ? end($field_values) : $field_values;
    }

    switch ($field->getType()) {
      case FieldsParamBuilder::AZURE_DATA_TYPE_DATE_TIME:
        $field_settings = $field->getDataDefinition()->getSettings();

        if (empty($field_settings) || !array_key_exists('datetime_type', $field_settings)) {
          // The field settings are empty, or the 'datetime_type' key doesn't
          // exist, so we assume a timestamp.
          $date = DrupalDateTime::createFromTimestamp($field_values);
        }
        else {
          // The 'datetime_type' key exists in the field settings, so we are
          // either working with a date, or a datetime.
          // So first determine the correct storage, before conversion.
          $storage_format = $field_settings['datetime_type'] === DateTimeItem::DATETIME_TYPE_DATE ? DateTimeItemInterface::DATE_STORAGE_FORMAT : DateTimeItemInterface::DATETIME_STORAGE_FORMAT;

          $date = DrupalDateTime::createFromFormat($storage_format, $field_values, DateTimeItemInterface::STORAGE_TIMEZONE);
        }

        // For a date/time offset, Azure expects an ISO-8601 standard.
        $field_values = $date->format('c');
        break;

      case FieldsParamBuilder::AZURE_DATA_TYPE_STRING_COLLECTION:
        // For a string-collection, Azure expects an array of strings.
        $field_values = array_map('strval', $field_values);
        break;

      case FieldsParamBuilder::AZURE_DATA_TYPE_INT_COLLECTION:
        // For an integer-collection, Azure expects an array of integers.
        $field_values = array_map('intval', $field_values);
        break;

      case 'text':
        // If the text field is empty, send an empty string to the server.
        if (empty($field_values)) {
          return '';
        }

        if ($field_values instanceof TextValue) {
          $field_values = $field_values->getText();
        }
        else {
          // Log text fields that are not empty, nor an instance of TextValue.
          $this->logger->notice($this->t('Could not create field values for field %field. An empty string was sent instead.', [
            '%field' => $field->getPropertyPath() ?? $field->getFieldIdentifier(),
          ]));

          $field_values = '';
        }

        break;
    }

    return $field_values;
  }

  /**
   * Build the document parameters for all items before indexing.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The Search API index.
   * @param \Drupal\search_api\Item\ItemInterface[] $items
   *   The items.
   *
   * @return array
   *   An array of parameters.
   */
  public function buildDocumentParams(IndexInterface $index, array $items): array {
    $build = [];

    foreach ($items as $item_id => $item) {
      $this->setDefaultFields($index, $item);

      $field_values = [];

      foreach ($item->getFields() as $field_name => $field) {
        $field_values[$field_name] = $this->createFieldValues($field);
      }

      $build[$item_id] = $field_values;
    }

    return $build;
  }

  /**
   * Sets the default items on a document before indexing.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The Search API index.
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The items to be indexed.
   */
  public function setDefaultFields(IndexInterface $index, ItemInterface $item): void {
    $item->setField('@search.action', $this->fieldsHelper
      ->createField($index, '@search.action', ['type' => 'string'])
      ->setValues(['mergeOrUpload']));

    $item->setField('id', $this->fieldsHelper
      ->createField($index, 'id', ['type' => 'string'])
      ->setValues([str_replace([':', '/'], '_', $item->getId())]));

    $item->setField('itemId', $this->fieldsHelper
      ->createField($index, 'itemId', ['type' => 'string'])
      ->setValues([$item->getId()]));

    $item->setField('search_api_id', $this->fieldsHelper
      ->createField($index, 'search_api_id', ['type' => 'string'])
      ->setValues([$item->getId()]));

    $item->setField('search_api_datasource', $this->fieldsHelper
      ->createField($index, 'search_api_datasource', ['type' => 'string'])
      ->setValues([$item->getDatasourceId()]));

    $item->setField('search_api_language', $this->fieldsHelper
      ->createField($index, 'search_api_language', ['type' => 'string'])
      ->setValues([$item->getLanguage()]));
  }

}
