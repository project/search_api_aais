<?php

namespace Drupal\search_api_aais\Azure\Fields;

use Drupal\search_api\DataType\DataTypePluginManager;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Utility\FieldsHelperInterface;

/**
 * Field parameter builder.
 */
class FieldsParamBuilder {

  /**
   * A mapping array between Search API and Azure field types.
   */
  const AZURE_DATA_TYPE_MAPPING = [
    'text' => 'Edm.String',
    'string' => 'Edm.String',
    'integer' => 'Edm.Int32',
    'decimal' => 'Edm.Double',
    'double' => 'Edm.Double',
    'boolean' => 'Edm.Boolean',
    'date' => 'Edm.Int64',
  ];

  /**
   * Other Azure data types.
   */
  const
    AZURE_DATA_TYPE_GEOGRAPHY_POINT = 'aais.geography_point',
    AZURE_DATA_TYPE_DATE_TIME = 'aais.date_time_offset',
    AZURE_DATA_TYPE_INT_COLLECTION = 'aais.integer_collection',
    AZURE_DATA_TYPE_STRING_COLLECTION = 'aais.string_collection';

  /**
   * The Azure data types.
   */
  const AZURE_DATA_TYPES = [
    self::AZURE_DATA_TYPE_GEOGRAPHY_POINT,
    self::AZURE_DATA_TYPE_DATE_TIME,
    self::AZURE_DATA_TYPE_INT_COLLECTION,
    self::AZURE_DATA_TYPE_STRING_COLLECTION,
  ];

  /**
   * The Data Type plugin manager.
   *
   * @var \Drupal\search_api\DataType\DataTypePluginManager
   */
  protected DataTypePluginManager $dataTypePluginManager;

  /**
   * The fields' helper.
   *
   * @var \Drupal\search_api\Utility\FieldsHelperInterface
   */
  protected FieldsHelperInterface $fieldsHelper;

  /**
   * Constructs the FieldsParamBuilder object.
   *
   * @param \Drupal\search_api\DataType\DataTypePluginManager $data_type_plugin_manager
   *   The Data Type plugin manager.
   * @param \Drupal\search_api\Utility\FieldsHelperInterface $fields_helper
   *   The field helper.
   */
  public function __construct(DataTypePluginManager $data_type_plugin_manager, FieldsHelperInterface $fields_helper) {
    $this->dataTypePluginManager = $data_type_plugin_manager;
    $this->fieldsHelper = $fields_helper;
  }

  /**
   * Build the field parameters.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The Search API index.
   *
   * @return array
   *   An array of fields.
   */
  public function buildFieldParams(IndexInterface $index): array {
    $build = [];

    $fields = $this->getDefaultFields($index) + $index->getFields();

    foreach ($fields as $field_id => $field) {
      $build[] = $this->createFieldMapping($field, $field_id === 'id');
    }

    return $build;
  }

  /**
   * Get the default fields.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search API index.
   *
   * @return \Drupal\search_api\Item\FieldInterface[]
   *   An array of fields.
   */
  public function getDefaultFields(IndexInterface $index): array {
    $fields = [];

    $fields['id'] = $this->fieldsHelper->createField($index, 'id', ['type' => 'string']);
    $fields['itemId'] = $this->fieldsHelper->createField($index, 'itemId', ['type' => 'string']);

    $fields['search_api_id'] = $this->fieldsHelper->createField($index, 'search_api_id', ['type' => 'string']);
    $fields['search_api_datasource'] = $this->fieldsHelper->createField($index, 'search_api_datasource', ['type' => 'string']);
    $fields['search_api_language'] = $this->fieldsHelper->createField($index, 'search_api_language', ['type' => 'string']);

    return $fields;
  }

  /**
   * Map fields to Azure AI Search fields.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The field.
   * @param bool $key
   *   Is this field the key for the document?
   *
   * @return array
   *   An array of mapped fields.
   *
   * @see https://learn.microsoft.com/en-us/azure/search/search-what-is-an-index
   */
  public function createFieldMapping(FieldInterface $field, bool $key = FALSE): array {
    $field_param = [
      'name' => $field->getFieldIdentifier(),
    ];

    $data_type_plugin = $this->dataTypePluginManager->getDefinition($field->getType());

    if (method_exists($data_type_plugin['class'], 'getAzureDataType')) {
      $field_param['type'] = call_user_func([$data_type_plugin['class'], 'getAzureDataType']);
    }
    elseif ($type = static::AZURE_DATA_TYPE_MAPPING[$field->getType()]) {
      $field_param['type'] = $type;
    }
    else {
      $field_param['type'] = static::AZURE_DATA_TYPE_MAPPING[$data_type_plugin['fallback_type']];
    }

    if ($key) {
      $field_param['key'] = TRUE;
    }

    if ($field_configuration = $field->getConfiguration()) {
      $field_attributes = self::getFieldAttributes();

      foreach ($field_configuration as $config_id => $config_value) {
        // Only pass attributes that Azure recognizes, as Drupal also sets field
        // attributes. E.g, it stores the role and/or view_mode on a
        // rendered_item.
        if (!array_key_exists($config_id, $field_attributes)) {
          continue;
        }

        $field_param[$config_id] = is_numeric($config_value) ? (bool) $config_value : $config_value;
      }
    }

    // The 'suggester' setting isn't an actual Azure attribute, so remove it.
    if (isset($field_param['suggester'])) {
      unset($field_param['suggester']);
    }

    return $field_param;
  }

  /**
   * Returns the Azure-specific field attributes.
   *
   * @return array
   *   An associative array of attributes with the identifier as the key, and
   *   the settings as the value.
   *   The settings are an associative array with the following keys:
   *   - label: The human-readable label for this attribute.
   *   - types: (optional) an associative array with the following keys:
   *     - allowed: an array of allowed field types for the attribute
   *     - disallowed: an array of disallowed field types for the attribute
   *   - changeable: whether the attribute can be changed after creation.
   *   - options: (optional) an array of options for the attribute.
   */
  public static function getFieldAttributes(): array {
    $attributes = [
      'searchable' => [
        'label' => t('Searchable'),
        'types' => [
          'allowed' => ['string', 'text', static::AZURE_DATA_TYPE_STRING_COLLECTION],
        ],
        'changeable' => FALSE,
        'description' => t('Indicates whether the field is full-text searchable and can be referenced in search queries.'),
      ],
      'filterable' => [
        'label' => t('Filterable'),
        'changeable' => FALSE,
        'description' => t('Indicates whether to enable the field to be referenced in filter queries.'),
      ],
      'sortable' => [
        'label' => t('Sortable'),
        'types' => [
          'disallowed' => [static::AZURE_DATA_TYPE_STRING_COLLECTION, static::AZURE_DATA_TYPE_INT_COLLECTION],
        ],
        'changeable' => FALSE,
        'description' => t('Indicates whether to enable the field to be referenced in order by expressions.'),
      ],
      'facetable' => [
        'label' => t('Facetable'),
        'types' => [
          'disallowed' => [static::AZURE_DATA_TYPE_GEOGRAPHY_POINT],
        ],
        'changeable' => FALSE,
        'description' => t('Indicates whether to enable the field to be referenced in facet queries.'),
      ],
      'retrievable' => [
        'label' => t('Retrievable'),
        'changeable' => TRUE,
        'description' => t('Indicates whether the field can be returned in a search result.'),
      ],
      'suggester' => [
        'label' => t('Suggester'),
        'types' => [
          'allowed' => ['string', 'text', static::AZURE_DATA_TYPE_STRING_COLLECTION],
        ],
        'changeable' => TRUE,
        'description' => t('Whether to add this field to the suggester source fields.'),
      ],
    ];

    if ($analyzers = self::getAnalyzers()) {
      // Add an empty option to the list of language analyzers.
      $analyzers = ['_none' => t('- None -')] + $analyzers;

      $attributes['analyzer'] = [
        'label' => t('Analyzer'),
        'types' => [
          'allowed' => ['string', 'text'],
        ],
        'options' => $analyzers,
        'changeable' => TRUE,
        'description' => t('Sets the lexical analyzer for tokenizing strings during indexing and query operations.'),
      ];
    }

    return $attributes;
  }

  /**
   * Returns the available analyzers.
   *
   * @return array
   *   An associative array of analyzers, keyed by the type of analyzer.
   *   Each analyzer is subsequently keyed by their setting, and has a
   *   human-readable label for its value.
   */
  public static function getAnalyzers(): array {
    $analyzers = [
      'built-in' => self::getBuiltInAnalyzers(),
      'language' => self::getLanguageAnalyzers(),
    ];

    // Allow to add some extra analyzers.
    \Drupal::moduleHandler()->alter('search_api_aais_analyzers', $analyzers);

    return $analyzers;
  }

  /**
   * Returns the available built-in analyzers.
   *
   * @return array
   *   An associative array of built-in analyzers.
   *
   * @see https://docs.microsoft.com/en-us/azure/search/index-add-custom-analyzers#built-in-analyzers
   */
  public static function getBuiltInAnalyzers(): array {
    return [
      'keyword' => t('Keyword'),
      'pattern' => t('Pattern'),
    ];
  }

  /**
   * Returns the available language analyzers.
   *
   * @return array
   *   An associative array of language analyzers.
   *
   * @see https://docs.microsoft.com/en-us/azure/search/index-add-language-analyzers#language-analyzer-list
   */
  public static function getLanguageAnalyzers(): array {
    return [
      'ar.microsoft' => t('Arabic'),
      'bn.microsoft' => t('Bangla'),
      'bg.microsoft' => t('Bulgarian'),
      'ca.microsoft' => t('Catalan'),
      'zh-Hans.microsoft' => t('Chinese Simplified'),
      'zh-Hant.microsoft' => t('Chinese Traditional'),
      'hr.microsoft' => t('Croatian'),
      'cs.microsoft' => t('Czech'),
      'da.microsoft' => t('Danish'),
      'nl.microsoft' => t('Dutch'),
      'en.microsoft' => t('English'),
      'et.microsoft' => t('Estonian'),
      'fi.microsoft' => t('Finnish'),
      'fr.microsoft' => t('French'),
      'de.microsoft' => t('German'),
      'el.microsoft' => t('Greek'),
      'gu.microsoft' => t('Gujarati'),
      'hu.microsoft' => t('Hungarian'),
      'is.microsoft' => t('Icelandic'),
      'id.microsoft' => t('Indonesian (Bahasa)'),
      'it.microsoft' => t('Italian'),
      'ja.microsoft' => t('Japanese'),
      'kn.microsoft' => t('Kannada'),
      'ko.microsoft' => t('Korean'),
      'lv.microsoft' => t('Latvian'),
      'lt.microsoft' => t('Lithuanian'),
      'ml.microsoft' => t('Malayalam'),
      'ms.microsoft' => t('Malay (Latin)'),
      'mr.microsoft' => t('Marathi'),
      'nb.microsoft' => t('Norwegian'),
      'pl.microsoft' => t('Polish'),
      'pt-Br.microsoft' => t('Portuguese (Brazil)'),
      'pt-Pt.microsoft' => t('Portuguese (Portugal)'),
      'pa.microsoft' => t('Punjabi'),
      'ro.microsoft' => t('Romanian'),
      'ru.microsoft' => t('Russian'),
      'sr-cyrillic.microsoft' => t('Serbian (Cyrillic)'),
      'sr-latin.microsoft' => t('Serbian (Latin)'),
      'sk.microsoft' => t('Slovak'),
      'sl.microsoft' => t('Slovenian'),
      'es.microsoft' => t('Spanish'),
      'sv.microsoft' => t('Swedish'),
      'ta.microsoft' => t('Tamil'),
      'te.microsoft' => t('Telugu'),
      'th.microsoft' => t('Thai'),
      'tr.microsoft' => t('Turkish'),
      'ur.microsoft' => t('Urdu'),
      'vi.microsoft' => t('Vietnamese'),
    ];
  }

}
