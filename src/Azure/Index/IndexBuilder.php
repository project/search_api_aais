<?php

namespace Drupal\search_api_aais\Azure\Index;

use Drupal\search_api\IndexInterface;
use Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder;

/**
 * Builder for the Search API index.
 */
class IndexBuilder {

  /**
   * The fields parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder
   */
  protected FieldsParamBuilder $fieldsParamBuilder;

  /**
   * Constructs an IndexBuilder.
   *
   * @param \Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder $fields_param_builder
   *   The fields' parameter builder.
   */
  public function __construct(FieldsParamBuilder $fields_param_builder) {
    $this->fieldsParamBuilder = $fields_param_builder;
  }

  /**
   * Build the index according the settings.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index.
   *
   * @return array
   *   The index build information.
   */
  public function buildIndex(IndexInterface $index): array {
    $index_name = static::getIndexName($index);

    $build = [
      'name' => $index_name,
      'fields' => $this->fieldsParamBuilder->buildFieldParams($index),
    ];

    $suggesters = [];
    foreach ($index->getFields() as $field) {
      $field_configuration = $field->getConfiguration();

      // Add scoring profiles.
      if ($field->getType() === 'text' && $field_configuration && $field_configuration['searchable']) {
        $build['scoringProfiles'][] = [
          'name' => $field->getFieldIdentifier(),
          'text' => [
            'weights' => [
              $field->getFieldIdentifier() => $field->getBoost(),
            ],
          ],
        ];
      }

      // Add the field to the suggester source fields list.
      if ($field_configuration['suggester']) {
        $suggester = $suggesters['sg'] ?? [
          'name' => 'sg',
          'searchMode' => 'analyzingInfixMatching',
          'sourceFields' => [],
        ];

        $suggester['sourceFields'][] = $field->getFieldIdentifier();
        $suggesters['sg'] = $suggester;
      }
    }

    $build['suggesters'] = array_values($suggesters);

    return $build;
  }

  /**
   * Gets a Search API Azure AI Search specific configuration value.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index from which to get the configuration value.
   * @param string|null $key
   *   (optional) The configuration key from which to get the value of.
   *
   * @return null|string|array
   *   The value(s) found for the configuration key, or NULL.
   */
  public static function getIndexConfiguration(IndexInterface $index, ?string $key = NULL): mixed {
    if ($key) {
      return $index->getThirdPartySetting('search_api_aais', $key);
    }
    else {
      return $index->getThirdPartySettings('search_api_aais');
    }
  }

  /**
   * Gets the name of an index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index of which to get the name.
   *
   * @return string
   *   The index name.
   */
  public static function getIndexName(IndexInterface $index): string {
    return self::getIndexConfiguration($index, 'index_name');
  }

  /**
   * Gets the fields on an index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index from which to get the fields.
   * @param bool $include_search_api_fields
   *   Whether to include the Search API-specific fields.
   *
   * @return \Drupal\search_api\Item\Field[]
   *   List of fields on the index.
   */
  public static function getIndexFields(IndexInterface $index, bool $include_search_api_fields = TRUE): array {
    $fields = $index->getFields();

    if ($include_search_api_fields) {
      /** @var \Drupal\search_api\Utility\FieldsHelper $fields_helper */
      $fields_helper = \Drupal::service('search_api.fields_helper');

      $fields['search_api_datasource'] = $fields_helper->createField($index, 'search_api_datasource', ['type' => 'string']);
      $fields['search_api_id'] = $fields_helper->createField($index, 'search_api_id', ['type' => 'string']);
      $fields['search_api_language'] = $fields_helper->createField($index, 'search_api_language', ['type' => 'string']);
      $fields['search_api_relevance'] = $fields_helper
        ->createField($index, 'search_api_relevance', ['type' => 'string'])
        ->setConfiguration(['sortable' => TRUE]);
    }

    return $fields;
  }

}
