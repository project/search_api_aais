<?php

namespace Drupal\search_api_aais\Azure\SynonymMap;

use Drupal\search_api\IndexInterface;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;

/**
 * Builder for the synonym map.
 */
class SynonymMapBuilder {

  /**
   * Suffix for the synonym map name.
   */
  const SUFFIX = '-synonym-map';

  const LIMIT = 5;

  /**
   * Builds the synonym map.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search API index.
   *
   * @return array
   *   The synonym map.
   *
   * @see https://learn.microsoft.com/en-us/rest/api/searchservice/create-synonym-map
   */
  public function buildSynonymMap(IndexInterface $index): array {
    return [
      'name' => static::getSynonymMapName($index),
      // Currently only solr supported.
      'format' => 'solr',
      // Synonym rules separated by the new line ('\n') character.
      'synonyms' => 'United States, United States of America, USA',
    ];
  }

  /**
   * Gets the synonym map name.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index for which to get the synonym map.
   *
   * @return string
   *   The synonym map name.
   */
  public static function getSynonymMapName(IndexInterface $index): string {
    return IndexBuilder::getIndexName($index) . static::SUFFIX;
  }

}
