<?php

namespace Drupal\search_api_aais\AzureAiSearch\Exception;

/**
 * Exception for invalid client configuration.
 */
class InvalidClientConfigurationException extends \RuntimeException {
}
