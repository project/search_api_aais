<?php

namespace Drupal\search_api_aais\Exception;

/**
 * Exception for invalid connector plugins.
 */
class InvalidConnectorException extends \RuntimeException {
}
