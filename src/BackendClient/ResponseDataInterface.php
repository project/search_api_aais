<?php

namespace Drupal\search_api_aais\BackendClient;

/**
 * An interface for response data.
 */
interface ResponseDataInterface {

  /**
   * Get the status code.
   *
   * @return int
   *   The HTTP status code.
   */
  public function getStatusCode(): int;

  /**
   * Sets the Status code.
   *
   * @param int $statusCode
   *   The HTTP status code.
   */
  public function setStatusCode(int $statusCode): void;

  /**
   * Get the response body.
   *
   * @return array|null
   *   The body of the response.
   */
  public function getBody(): ?array;

  /**
   * Sets the response body.
   *
   * @param array|null $body
   *   The body of the response.
   */
  public function setBody(?array $body): void;

}
