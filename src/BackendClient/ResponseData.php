<?php

namespace Drupal\search_api_aais\BackendClient;

use Drupal\Component\Serialization\Json;
use Psr\Http\Message\ResponseInterface;

/**
 * Class to hold the response data.
 */
class ResponseData implements ResponseDataInterface {

  /**
   * The status code of the response.
   *
   * @var int
   */
  protected int $statusCode;

  /**
   * The body.
   *
   * @var array|null
   */
  protected ?array $body;

  /**
   * {@inheritDoc}
   */
  public function getStatusCode(): int {
    return $this->statusCode;
  }

  /**
   * {@inheritDoc}
   */
  public function setStatusCode(int $statusCode): void {
    $this->statusCode = $statusCode;
  }

  /**
   * {@inheritDoc}
   */
  public function getBody(): ?array {
    return $this->body;
  }

  /**
   * {@inheritDoc}
   */
  public function setBody(?array $body): void {
    $this->body = $body;
  }

  /**
   * Was the request successful?
   *
   * @return bool
   *   Has the request successfully completed?
   */
  public function isSuccessful(): bool {
    return $this->statusCode === 200;
  }

  /**
   * Constructs a response data object from a PSR response object.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   A PSR-compliant response object.
   *
   * @return static
   *   A response data object.
   */
  public static function fromResponse(ResponseInterface $response) : ResponseData {
    $response_data = new static();
    $response_data->setStatusCode($response->getStatusCode());
    $response_data->setBody(Json::decode($response->getBody()));

    return $response_data;
  }

}
