<?php

namespace Drupal\search_api_aais\BackendClient;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_aais\Azure\Documents\DocumentsParamBuilder;
use Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;
use Drupal\search_api_aais\Azure\Query\QueryParamBuilder;
use Drupal\search_api_aais\Azure\Query\QueryResultParser;
use Drupal\search_api_aais\Azure\SynonymMap\SynonymMapBuilder;
use Drupal\search_api_aais\Form\AaisConfigForm;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

/**
 * A client to communicate with the Azure AI Search API.
 */
class BackendClient implements BackendClientInterface {

  use StringTranslationTrait;

  const SUPPORTED_METHODS = ['GET', 'POST', 'PUT', 'DELETE'];

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The UUID service.
   *
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   */
  protected UuidInterface $uuid;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The URL of the API.
   *
   * @var string
   */
  protected string $url;

  /**
   * The API key.
   *
   * @var string|null
   */
  protected ?string $apiKey;

  /**
   * The API version.
   *
   * @var string|null
   */
  protected ?string $version;

  /**
   * The query type.
   *
   * @var string|null
   */
  protected ?string $queryType;

  /**
   * The answer configuration.
   *
   * @var string|null
   */
  protected ?string $answer;

  /**
   * The query parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryParamBuilder
   */
  protected QueryParamBuilder $queryParamBuilder;

  /**
   * The query result parser.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryResultParser
   */
  protected QueryResultParser $queryResultParser;

  /**
   * The fields parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder
   */
  protected FieldsParamBuilder $fieldsParamBuilder;

  /**
   * The index builder.
   *
   * @var \Drupal\search_api_aais\Azure\Index\IndexBuilder
   */
  protected IndexBuilder $indexBuilder;

  /**
   * The synonym map builder.
   *
   * @var \Drupal\search_api_aais\Azure\SynonymMap\SynonymMapBuilder
   */
  protected SynonymMapBuilder $synonymMapBuilder;

  /**
   * The documents' parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Documents\DocumentsParamBuilder
   */
  protected DocumentsParamBuilder $documentsParamBuilder;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\search_api_aais\Azure\Query\QueryParamBuilder $query_param_builder
   *   The query parameter builder.
   * @param \Drupal\search_api_aais\Azure\Query\QueryResultParser $query_result_parser
   *   The query result parser.
   * @param \Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder $fields_param_builder
   *   The fields parameter builder.
   * @param \Drupal\search_api_aais\Azure\Index\IndexBuilder $index_builder
   *   The index builder.
   * @param \Drupal\search_api_aais\Azure\SynonymMap\SynonymMapBuilder $synonym_map_builder
   *   The synonym map builder.
   * @param \Drupal\search_api_aais\Azure\Documents\DocumentsParamBuilder $documents_param_builder
   *   The params builder.
   * @param string $url
   *   The remote server URL.
   * @param string $version
   *   The API version.
   * @param string $query_type
   *   The query type.
   * @param string $answer
   *   The answer type.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UuidInterface $uuid, LoggerInterface $logger, ClientInterface $http_client, QueryParamBuilder $query_param_builder, QueryResultParser $query_result_parser, FieldsParamBuilder $fields_param_builder, IndexBuilder $index_builder, SynonymMapBuilder $synonym_map_builder, DocumentsParamBuilder $documents_param_builder, string $url, string $version = '2020-06-30', string $query_type = 'Semantic', string $answer = 'extractive|count-1') {
    $this->configFactory = $config_factory;
    $this->uuid = $uuid;
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->queryParamBuilder = $query_param_builder;
    $this->queryResultParser = $query_result_parser;
    $this->fieldsParamBuilder = $fields_param_builder;
    $this->indexBuilder = $index_builder;
    $this->synonymMapBuilder = $synonym_map_builder;
    $this->documentsParamBuilder = $documents_param_builder;

    $this->url = $url;
    $this->version = $version;
    $this->queryType = $query_type;
    $this->answer = $answer;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiKey(string $api_key): void {
    $this->apiKey = $api_key;
  }

  /**
   * Create a complete URL ready for use.
   *
   * @param string $path
   *   The path to the endpoint.
   * @param array $http_options
   *   A list of HTTP options.
   *
   * @return string
   *   A complete endpoint URL
   */
  protected function buildUrl(string $path, array $http_options = []) : string {
    $http_options = [
      'api-version' => $this->version,
    ] + $http_options;

    return rtrim($this->url, '/') . '/' . ltrim($path, '/') . '?' . http_build_query($http_options);
  }

  /**
   * Validate if the HTTP method is valid.
   *
   * @param string $method
   *   The HTTP method.
   *
   * @throws \InvalidArgumentException
   *   An invalid method has been supplied.
   */
  protected function validateMethod(string $method): void {
    if (!in_array($method, self::SUPPORTED_METHODS, TRUE)) {
      throw new \InvalidArgumentException('The used method is not supported.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function request(string $path, string $method = 'GET', array $options = []): ResponseData {
    $url = $this->buildUrl($path);
    $this->validateMethod(strtoupper($method));

    // When an API key is supplied, add it.
    if ($this->apiKey) {
      if (!isset($options['headers'])) {
        $options['headers'] = [];
      }
      $options['headers'] += ['api-key' => $this->apiKey];
    }

    try {
      $response = $this->httpClient->request($method, $url, $options);
      $response_data = ResponseData::fromResponse($response);
    }
    catch (ClientException | RequestException $e) {
      $status_code = $e->getResponse() ? $e->getResponse()->getStatusCode() : 500;

      // Catch & log all exceptions, except for 404's.
      switch ($status_code) {
        case 404:
          // We do not log 404's for the time being, as in some situations a
          // 404 is actually the desired result.
          // When trying to create a remote index for example, we need to know
          // if the index already exists or not. To do this, we need to send a
          // GET-request, and only actually proceed with creating the remote
          // index if the request returns a 404.
          // @todo Filter on request type. Log when not expected.
          break;

        default:
          // In case of a ClientException, try and log the exception in a
          // readable format.
          // See https://learn.microsoft.com/en-us/rest/api/searchservice/http-status-codes
          // for more info on specific status codes.
          // @todo Can be simplified.
          if ($e instanceof ClientException) {
            try {
              $e_contents = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
              if ($e_contents) {
                if (array_key_exists('details', $e_contents['error'])) {
                  $this->logger->error(implode(': ', $e_contents['error']['details'][0]));
                }
                elseif (array_key_exists('message', $e_contents['error'])) {
                  $this->logger->error($e_contents['error']['message']);
                }
              }
              else {
                $this->logger->error($e->getMessage());
              }
            }
            catch (\Error $x) {
              $this->logger->error($x->getMessage());
            }
          }
          else {
            $this->logger->error($e->getMessage());
          }
          break;
      }

      $response_data = new ResponseData();
      $response_data->setStatusCode($status_code);
    }
    catch (ConnectException $e) {
      $this->logger->error($e->getMessage());

      $response_data = new ResponseData();
      $response_data->setStatusCode(400);
    }

    // If configured, log the query information that was just sent to Azure.
    if ($this->configFactory->get(AaisConfigForm::SETTINGS)->get('log_queries')) {
      // Generate a UUID, so we can link the request and the response.
      $uuid = $this->uuid->generate();

      // Escape the highlight tags when set, so they will be logged correctly.
      if (isset($options['json']['highlightPreTag'])) {
        $options['json']['highlightPreTag'] = htmlentities($options['json']['highlightPreTag']);
        $options['json']['highlightPostTag'] = htmlentities($options['json']['highlightPostTag']);
      }

      // Log the request.
      $this->logger->info('<pre>' . print_r([
        'uuid' => $uuid,
        'method' => $method,
        'url' => $url,
        'options' => $options,
      ], TRUE) . '</pre>');

      // Log the response, if successful. If not, the standard logging will show
      // what went wrong.
      if ($response_data->isSuccessful()) {
        $this->logger->info('<pre>' . print_r(['uuid' => $uuid] + $response_data->getBody(), TRUE) . '</pre>');
      }
    }

    // Return an error response.
    return $response_data;
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query): void {
    $index = $query->getIndex();
    $index_name = $index->getThirdPartySetting('search_api_aais', 'index_name');

    // Build our query parameters.
    $params = $this->queryParamBuilder->buildQueryParams($query);

    $response = $this->request('indexes/' . $index_name . '/docs/search', 'POST', [
      'json' => $params,
    ]);

    if (!$response->isSuccessful()) {
      throw new SearchApiException(sprintf('Error querying index %s', $index->id()));
    }

    // Parse the returning result set.
    $this->queryResultParser->parseResult($query, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index): void {
    if ($this->indexExists($index)) {
      return;
    }

    $this->buildIndex($index);
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index): void {
    if ($this->indexRemove($index)) {
      $index->clear();
      $this->removeIndex($index);
    }

    $this->buildIndex($index);
  }

  /**
   * {@inheritdoc}
   */
  public function clearIndex(IndexInterface $index, ?string $datasource_id = NULL): void {
    $this->removeIndex($index);
    $this->addIndex($index);
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items): array {
    if (empty($items)) {
      return [];
    }

    // Keep track of the indexed items.
    $indexed = [];

    // Build the parameters.
    $params = $this->documentsParamBuilder->buildDocumentParams($index, $items);

    $response_data = $this->request('indexes/' . IndexBuilder::getIndexName($index) . '/docs/index', 'POST', [
      'json' => [
        'value' => array_values($params),
      ],
    ]);

    if ($response_data->isSuccessful()) {
      $indexed_items = $response_data->getBody()['value'];

      foreach ($indexed_items as $indexed_item) {
        if ($indexed_item['statusCode'] === 200 || $indexed_item['statusCode'] === 201) {
          // Map the returned Azure item ID's back to their Drupal item ID
          // equivalents, so that the Index tracker correctly tracks the
          // status of each indexed item.
          if ($item_id = array_search($indexed_item['key'], array_combine(array_keys($params), array_column($params, 'id')))) {
            $indexed[] = $item_id;
          }
        }
      }
    }

    return $indexed;
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex(IndexInterface $index): void {
    $index_name = IndexBuilder::getIndexName($index);

    $this->request('indexes/' . $index_name, 'DELETE');
  }

  /**
   * {@inheritdoc}
   */
  public function indexExists(IndexInterface|string $index): bool {
    $index = ($index instanceof IndexInterface) ? IndexBuilder::getIndexName($index) : $index;

    $response = $this->request('indexes/' . $index);

    return $response->isSuccessful();
  }

  /**
   * {@inheritdoc}
   */
  public function indexRemove(IndexInterface $index): bool {
    $index_name = IndexBuilder::getIndexName($index);

    // If the index does not currently exist (or it is new), there is no need
    // for removal.
    if (!$this->indexExists($index_name) || !isset($index->original)) {
      return FALSE;
    }

    // Check for updates to the various configuration options of the index.
    $index_configuration_current = $this->indexBuilder->buildIndex($index);
    $index_configuration_original = $this->indexBuilder->buildIndex($index->original);

    if ($index_configuration_current !== $index_configuration_original) {
      // The build configuration of the updated index differs from the original
      // index, so we assume a removal of the old index is needed.
      // @todo Tweak this as needed.
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function addSynonymMap(IndexInterface $index): void {
    $this->request('synonymmaps/' . SynonymMapBuilder::getSynonymMapName($index), 'PUT', [
      'json' => $this->synonymMapBuilder->buildSynonymMap($index),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function listSynonymMaps(): array {
    $synonym_maps = [];

    $response = $this->request('synonymmaps');

    if ($response->isSuccessful()) {
      $synonym_maps = $response->getBody()['value'];
    }

    return $synonym_maps;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSynonymMap(IndexInterface $index): void {
    $this->request('synonymmaps/' . SynonymMapBuilder::getSynonymMapName($index), 'DELETE');
  }

  /**
   * {@inheritdoc}
   */
  public function buildIndex(IndexInterface $index): void {
    $index_name = IndexBuilder::getIndexName($index);

    $build = $this->indexBuilder->buildIndex($index);

    $this->request('indexes/' . $index_name, 'PUT', [
      'json' => $build,
    ]);

    // Check for any updates to the synonym map.
    $settings_current = IndexBuilder::getIndexConfiguration($index);
    $settings_original = isset($index->original) ? IndexBuilder::getIndexConfiguration($index->original) : [];

    // Filter the arrays to only keep the keys we are interested in and that can
    // have an impact on the synonym map.
    $relevant_keys = ['index_name', 'synonym_map', 'semantic_configuration'];
    $filter_callback = fn ($key) => in_array($key, $relevant_keys);
    $settings_original = array_filter($settings_original, $filter_callback, ARRAY_FILTER_USE_KEY);
    $settings_current = array_filter($settings_current, $filter_callback, ARRAY_FILTER_USE_KEY);

    // Compare the updated settings with the original ones, as that will
    // determine if we need to add the synonym map, or remove it.
    $settings_updated = array_diff($settings_current, $settings_original);

    if (empty($settings_original)) {
      if (!((isset($settings_updated['synonym_map']) && $settings_updated['synonym_map'] === 0))) {
        // A new index is being created and the synonym_map option was checked,
        // so create a new synonym map.
        $this->addSynonymMap($index);
      }
    }
    else {
      if (!empty($settings_updated)) {
        if (isset($settings_updated['synonym_map']) && $settings_updated['synonym_map'] === 1) {
          // The synonym_map option was checked on an existing index, so add a
          // new synonym map.
          $this->addSynonymMap($index);
        }
        else {
          // The synonym_map option was unchecked on an existing index, so
          // remove the synonym map.
          $this->deleteSynonymMap($index);
        }
      }
    }
  }

}
