<?php

namespace Drupal\search_api_aais\BackendClient;

use Drupal\search_api\IndexInterface;
use Drupal\search_api\Query\QueryInterface;

/**
 * An interface for an Azure AI Search API client.
 */
interface BackendClientInterface {

  /**
   * Set the API key.
   *
   * @param string $api_key
   *   An API key.
   */
  public function setApiKey(string $api_key): void;

  /**
   * Adds a new index to this server.
   *
   * If the index was already added to the server, the object should treat this
   * as if removeIndex() and then addIndex() were called.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index to add.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred while adding the index.
   */
  public function addIndex(IndexInterface $index): void;

  /**
   * Builds an index to this server.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index to add.
   */
  public function buildIndex(IndexInterface $index): void;

  /**
   * Deletes all the items from the index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index for which items should be deleted.
   * @param string|null $datasource_id
   *   (optional) If given, only delete items from the datasource with the
   *   given ID.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred while trying to delete indexed items.
   */
  public function clearIndex(IndexInterface $index, ?string $datasource_id = NULL): void;

  /**
   * Checks if an index exists on the server.
   *
   * @param string|\Drupal\search_api\IndexInterface $index
   *   The index name, or an index object to derive it from.
   *
   * @return bool
   *   Whether the index exists.
   */
  public function indexExists(IndexInterface|string $index): bool;

  /**
   * Checks if an index needs to be removed due to a configuration change.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index to remove.
   *
   * @return bool
   *   Whether the index needs to be removed.
   */
  public function indexRemove(IndexInterface $index): bool;

  /**
   * Indexes the specified items.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search index for which items should be indexed.
   * @param \Drupal\search_api\Item\ItemInterface[] $items
   *   An array of items to be indexed, keyed by their item IDs.
   *
   * @return string[]
   *   The IDs of all items that were successfully indexed.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if indexing was prevented by a fundamental configuration error.
   */
  public function indexItems(IndexInterface $index, array $items): array;

  /**
   * Removes an index from the server.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The Search API index.
   */
  public function removeIndex(IndexInterface $index): void;

  /**
   * Executes a search on this server.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The query to execute.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error prevented the search from completing.
   */
  public function search(QueryInterface $query);

  /**
   * Notifies the server that an index attached to it has been changed.
   *
   * If any user action is necessary as a result of this, the method should
   * set a message to notify the user.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The updated index.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred while reacting to the change.
   */
  public function updateIndex(IndexInterface $index);

  /**
   * Adds or updates a synonym map on the server.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index for which to add the synonym map.
   *
   * @see https://docs.microsoft.com/en-us/rest/api/searchservice/synonym-map-operations
   */
  public function addSynonymMap(IndexInterface $index): void;

  /**
   * Deletes a synonym map from this server.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index the synonym map belongs to.
   *
   * @see https://docs.microsoft.com/en-us/rest/api/searchservice/synonym-map-operations
   */
  public function deleteSynonymMap(IndexInterface $index): void;

  /**
   * Lists the synonym maps on the server.
   *
   * @return string[]
   *   The synonym maps found on the server.
   */
  public function listSynonymMaps(): array;

  /**
   * Executes an API request on this client.
   *
   * @param string $path
   *   The path of the endpoint.
   * @param string $method
   *   The HTTP method to use.
   * @param array $options
   *   The options' data.
   *
   * @return \Drupal\search_api_aais\BackendClient\ResponseData
   *   A response data object.
   *
   * @throws \InvalidArgumentException
   *   An invalid method has been supplied.
   */
  public function request(string $path, string $method = 'GET', array $options = []): ResponseData;

}
