<?php

namespace Drupal\search_api_aais\BackendClient;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\search_api_aais\Azure\Documents\DocumentsParamBuilder;
use Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;
use Drupal\search_api_aais\Azure\Query\QueryParamBuilder;
use Drupal\search_api_aais\Azure\Query\QueryResultParser;
use Drupal\search_api_aais\Azure\SynonymMap\SynonymMapBuilder;
use Drupal\search_api_aais\AzureAiSearch\Exception\InvalidClientConfigurationException;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * A factory to create a client to use with the Azure AI Search API.
 */
class BackendClientFactory {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The UUID service.
   *
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   */
  protected UuidInterface $uuid;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The query parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryParamBuilder
   */
  protected QueryParamBuilder $queryParamBuilder;

  /**
   * The query result parser.
   *
   * @var \Drupal\search_api_aais\Azure\Query\QueryResultParser
   */
  protected QueryResultParser $queryResultParser;

  /**
   * The fields parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder
   */
  protected FieldsParamBuilder $fieldsParamBuilder;

  /**
   * The index builder.
   *
   * @var \Drupal\search_api_aais\Azure\Index\IndexBuilder
   */
  protected IndexBuilder $indexBuilder;

  /**
   * The synonym map builder.
   *
   * @var \Drupal\search_api_aais\Azure\SynonymMap\SynonymMapBuilder
   */
  protected SynonymMapBuilder $synonymMapBuilder;

  /**
   * The document parameter builder.
   *
   * @var \Drupal\search_api_aais\Azure\Documents\DocumentsParamBuilder
   */
  protected DocumentsParamBuilder $documentsParamBuilder;

  /**
   * Creates a backend client factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\search_api_aais\Azure\Query\QueryParamBuilder $query_param_builder
   *   The query parameter builder.
   * @param \Drupal\search_api_aais\Azure\Query\QueryResultParser $query_result_parser
   *   The query result parser.
   * @param \Drupal\search_api_aais\Azure\Fields\FieldsParamBuilder $fields_param_builder
   *   The fields parameter builder.
   * @param \Drupal\search_api_aais\Azure\Index\IndexBuilder $index_builder
   *   The index builder.
   * @param \Drupal\search_api_aais\Azure\SynonymMap\SynonymMapBuilder $synonym_map_builder
   *   The synonym map builder.
   * @param \Drupal\search_api_aais\Azure\Documents\DocumentsParamBuilder $documents_param_builder
   *   The synonym map builder.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UuidInterface $uuid, LoggerInterface $logger, ClientInterface $http_client, QueryParamBuilder $query_param_builder, QueryResultParser $query_result_parser, FieldsParamBuilder $fields_param_builder, IndexBuilder $index_builder, SynonymMapBuilder $synonym_map_builder, DocumentsParamBuilder $documents_param_builder) {
    $this->configFactory = $config_factory;
    $this->uuid = $uuid;
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->queryParamBuilder = $query_param_builder;
    $this->queryResultParser = $query_result_parser;
    $this->fieldsParamBuilder = $fields_param_builder;
    $this->indexBuilder = $index_builder;
    $this->synonymMapBuilder = $synonym_map_builder;
    $this->documentsParamBuilder = $documents_param_builder;
  }

  /**
   * Creates a new Azure AI Search API client.
   *
   * @param array $configuration
   *   The shared settings configuration to pass to the client.
   *
   * @return \Drupal\search_api_aais\BackendClient\BackendClientInterface
   *   The Azure AI Search client.
   */
  public function create(array $configuration): BackendClientInterface {
    if (!isset($configuration['url']) || !isset($configuration['api_version'])) {
      throw new InvalidClientConfigurationException('Missing required arguments.');
    }

    // The factory can be overkill right now. When an Azure client gets
    // available as composer package, load it here.
    return new BackendClient(
      $this->configFactory,
      $this->uuid,
      $this->logger,
      $this->httpClient,
      $this->queryParamBuilder,
      $this->queryResultParser,
      $this->fieldsParamBuilder,
      $this->indexBuilder,
      $this->synonymMapBuilder,
      $this->documentsParamBuilder,
      $configuration['url'],
      $configuration['api_version'],
    );
  }

}
