<?php

namespace Drupal\search_api_aais\Plugin\AzureAiSearch\Connector;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_aais\BackendClient\BackendClientInterface;
use Drupal\search_api_aais\Connector\AzureAiSearchConnectorInterface;
use Drupal\search_api_aais\Connector\AzureAiSearchConnectorPluginBase;

/**
 * Provides a standard Azure AI Search connector.
 *
 * @AzureAiSearchConnector(
 *   id = "standard",
 *   label = @Translation("Standard (API key)"),
 *   description = @Translation("The standard connector with API key.")
 * )
 */
class StandardConnector extends AzureAiSearchConnectorPluginBase implements AzureAiSearchConnectorInterface {

  /**
   * Get the API key.
   *
   * @return string
   *   The configured API key.
   */
  public function getApiKey(): string {
    return (string) $this->configuration['api_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'api_key' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_key'] = [
      '#title' => $this->t('API key'),
      '#description' => $this->t('Your API key to use the Azure AI Search API. For now, <u>only admin API keys are supported!</u>'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->configuration['api_key'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['api_key'] = $form_state->getValue('api_key');
  }

  /**
   * Gets the API client.
   *
   * @return \Drupal\search_api_aais\BackendClient\BackendClientInterface
   *   The API client.
   */
  public function createClient(array &$configuration): BackendClientInterface {
    $client = parent::createClient($configuration);
    $client->setApiKey($this->configuration['api_key']);

    return $client;
  }

}
