<?php

namespace Drupal\search_api_aais\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\StringDataType;

/**
 * Provides a Geography Point data type.
 *
 * @SearchApiDataType(
 *   id = "aais.geography_point",
 *   label = @Translation("Geography Point"),
 *   description = @Translation("A point representing a geographic location on the globe."),
 *   fallback_type = "string"
 * )
 */
class GeographyPointDataType extends StringDataType implements AzureDataTypeInterface {

  /**
   * {@inheritdoc}
   */
  public static function getAzureDataType(): string {
    return 'Edm.GeographyPoint';
  }

  /**
   * {@inheritdoc}
   */
  public static function isCollection(): bool {
    return FALSE;
  }

}
