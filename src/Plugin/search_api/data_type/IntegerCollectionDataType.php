<?php

namespace Drupal\search_api_aais\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\StringDataType;

/**
 * Provides an Integer Collection data type.
 *
 * @SearchApiDataType(
 *   id = "aais.integer_collection",
 *   label = @Translation("Integer collection"),
 *   description = @Translation("A list of integer values."),
 *   fallback_type = "string"
 * )
 */
class IntegerCollectionDataType extends StringDataType implements AzureDataTypeInterface {

  /**
   * {@inheritdoc}
   */
  public static function getAzureDataType(): string {
    return 'Collection(Edm.Int64)';
  }

  /**
   * {@inheritdoc}
   */
  public static function isCollection(): bool {
    return TRUE;
  }

}
