<?php

namespace Drupal\search_api_aais\Plugin\search_api\data_type;

/**
 * Interface for Azure data types.
 */
interface AzureDataTypeInterface {

  /**
   * Returns the Azure data type.
   *
   * @see https://docs.microsoft.com/en-us/rest/api/searchservice/supported-data-types
   *
   * @return string
   *   The matching Azure data type.
   */
  public static function getAzureDataType(): string;

  /**
   * Returns whether the data type is a collection.
   *
   * @return bool
   *   TRUE if the data type is a collection, FALSE otherwise.
   */
  public static function isCollection(): bool;

}
