<?php

namespace Drupal\search_api_aais\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\StringDataType;

/**
 * Provides a Date Time offset data type.
 *
 * @SearchApiDataType(
 *   id = "aais.date_time_offset",
 *   label = @Translation("Date Time offset"),
 *   description = @Translation("Date and time values represented in the OData V4 format."),
 *   fallback_type = "string"
 * )
 */
class DateTimeOffsetDataType extends StringDataType implements AzureDataTypeInterface {

  /**
   * {@inheritdoc}
   */
  public static function getAzureDataType(): string {
    return 'Edm.DateTimeOffset';
  }

  /**
   * {@inheritdoc}
   */
  public static function isCollection(): bool {
    return FALSE;
  }

}
