<?php

namespace Drupal\search_api_aais\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\StringDataType;

/**
 * Provides a String Collection data type.
 *
 * @SearchApiDataType(
 *   id = "aais.string_collection",
 *   label = @Translation("String collection"),
 *   description = @Translation("A list of strings."),
 *   fallback_type = "string"
 * )
 */
class StringCollectionDataType extends StringDataType implements AzureDataTypeInterface {

  /**
   * {@inheritdoc}
   */
  public static function getAzureDataType(): string {
    return 'Collection(Edm.String)';
  }

  /**
   * {@inheritdoc}
   */
  public static function isCollection(): bool {
    return TRUE;
  }

}
