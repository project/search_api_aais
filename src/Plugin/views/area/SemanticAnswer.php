<?php

namespace Drupal\search_api_aais\Plugin\views\area;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\search_api_aais\Azure\Index\IndexBuilder;
use Drupal\search_api_aais\Utility;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area handler to display the Azure semantic answers.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("semantic_answer")
 */
class SemanticAnswer extends AreaPluginBase {

  /**
   * Constructs a new SemanticAnswer.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    $options['semantic_scoring'] = [
      'default' => 0,
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    if ($this->moduleHandler->moduleExists('search_api_aais_logging')) {
      $form['semantic_scoring'] = [
        '#title' => $this->t('Semantic scoring'),
        '#type' => 'checkbox',
        '#default_value' => $this->options['semantic_scoring'],
        '#description' => $this->t('Enable up/down-voting of semantic answers.'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE): array {
    $semantic_answers = [];
    $search_results = NULL;
    $query = $this->view->getQuery();

    if ($query instanceof SearchApiQuery) {
      $search_results = $query->getSearchApiResults();
      $answers = $search_results->getExtraData('search_aais.answers');

      if (!empty($answers)) {
        // Get the index and its settings.
        $index = $query->getIndex();
        $index_settings = IndexBuilder::getIndexConfiguration($index);

        foreach ($answers as $answer) {
          $semantic_answer = [
            'uuid' => $search_results->getExtraData('search_aais.uuid') ?? NULL,
            'key' => $answer['key'],
            'entity_uri' => Utility::itemIdToEntityUri($answer['key']),
            'score' => $answer['score'],
            'text' => $answer['text'] ? ['#markup' => $answer['text']] : NULL,
            'highlights' => $answer['highlights'] ? ['#markup' => $answer['highlights']] : NULL,
          ];

          // Prepare the HTML-attributes for the semantic answer.
          $attributes = new Attribute();
          $attributes->setAttribute('id', 'semantic-answer-' . $semantic_answer['key']);

          // Add the UUID of the search operation, as well as the required JS
          // classes for the logging if it's enabled.
          if ($uuid = $semantic_answer['uuid']) {
            $attributes->setAttribute('data-aais-uuid', $uuid);

            if ($index_settings['search_api_aais_logging']['tracking']) {
              $attributes->setAttribute('data-aais-logging-uri', Utility::itemIdToEntityUri($answer['key'], TRUE));

              // The semantic answer uses the special index of 0.
              $attributes->setAttribute('data-aais-logging-index', 0);
            }
          }

          $semantic_answer['attributes'] = $attributes;
          $semantic_answers[] = $semantic_answer;
        }
      }
    }

    return [
      '#theme' => 'semantic_answers',
      '#answers' => $semantic_answers,
      '#semantic_scoring' => $this->options['semantic_scoring'],
      // Add the initial search result set, so other modules can act on the
      // extra data that might be present.
      '#search_results' => $search_results,
    ];
  }

}
