<?php

namespace Drupal\search_api_aais\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a connector plugin annotation object.
 *
 * Condition plugins provide generalized conditions for use in other
 * operations, such as conditional block placement.
 *
 * Plugin Namespace: Plugin\AzureAiSearch
 *
 * @see \Drupal\search_api_aais\Connector\ConnectorPluginManager
 * @see \Drupal\search_api_aais\Connector\AzureAiSearchConnectorInterface
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class AzureAiSearchConnector extends Plugin {

  /**
   * The Azure AI Search connector plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the Azure AI Search connector.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The backend description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

}
