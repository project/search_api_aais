<?php

namespace Drupal\search_api_aais;

use Drupal\search_api\Contrib\AutocompleteBackendInterface;
use Drupal\search_api_aais\BackendClient\BackendClientInterface;

/**
 * Defines an interface for Azure AI Search backend plugins.
 *
 * It extends the generic \Drupal\search_api\Backend\BackendInterface and covers
 * additional Azure AI Search specific methods.
 */
interface AzureAiSearchBackendInterface extends AutocompleteBackendInterface {

  /**
   * The minimal API version required for semantic searches.
   */
  public const MINIMAL_SEMANTIC_CONFIGURATION_VERSION = '2021-04-30-Preview';

  /**
   * Determines whether the backend requires semantic search configuration.
   *
   * @return bool
   *   TRUE if the backend requires semantic search configuration.
   */
  public function requiresSemanticConfiguration(): bool;

  /**
   * Determines whether the API-version is considered to be in 'preview' mode.
   *
   * @return bool
   *   Whether the API-version is considered to be in 'preview' mode.
   */
  public function inPreviewMode(): bool;

  /**
   * Gets the Azure AI Search API client.
   *
   * @return \Drupal\search_api_aais\BackendClient\BackendClientInterface
   *   The Azure AI Search API client.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getClient(): BackendClientInterface;

}
