<?php

namespace Drupal\Tests\search_api_aais\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\search_api_aais\Utility;

/**
 * Tests the Search API Azure AI Search utility class.
 *
 * @coversDefaultClass \Drupal\search_api_aais\Utility
 *
 * @group search_api_aais
 */
class UtilityTest extends UnitTestCase {

  /**
   * Test the itemIdToEntityUri() function.
   *
   * @dataProvider itemIdToEntityUriProvider
   */
  public function testItemIdToEntityUri($expected, $value) {
    $this->assertEquals($expected, Utility::itemIdToEntityUri($value));
  }

  /**
   * Test the itemIdToEntityUri() function with langcode.
   *
   * @dataProvider itemIdToEntityUriWithLangcodeProvider
   */
  public function testItemIdToEntityUriWithLangcode($expected, $value) {
    $this->assertEquals($expected, Utility::itemIdToEntityUri($value, TRUE));
  }

  /**
   * Provides values for testing the itemIdToEntityUri() methods.
   *
   * @return array[]
   *   A multidimensional array containing the values.
   */
  public static function itemIdToEntityUriProvider(): array {
    return [
      ['entity:node/1', 'entity_node_1_en'],
      ['entity:taxonomy_term/1', 'entity_taxonomy_term_1_en'],
      ['entity:some_entity_type/1', 'entity_some_entity_type_1_en'],
    ];
  }

  /**
   * Provides values for testing the itemIdToEntityUri() methods.
   *
   * @return array[]
   *   A multidimensional array containing the values.
   */
  public static function itemIdToEntityUriWithLangcodeProvider(): array {
    return [
      ['entity:node/1:en', 'entity_node_1_en'],
      ['entity:taxonomy_term/1:en', 'entity_taxonomy_term_1_en'],
      ['entity:some_entity_type/1:en', 'entity_some_entity_type_1_en'],
    ];
  }

}
